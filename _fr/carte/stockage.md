---
title: Stockage et métadonnées
layout: default
root: ../..
idx: 3.3
description: Mod stockage, NodeMetaRef (get_meta).
redirect_from:
  - /en/chapters/node_metadata.html
  - /en/map/node_metadata.html
---

## Introduction <!-- omit in toc -->

Dans ce chapitre, vous apprendrez comment stocker des données.

- [métadonnées](#metadata)
  - [Qu'est-ce que les métadonnées ?](#what-is-metadata)
  - [Obtenir les métadonnées d'un objet](#obtaining-a-metadata-object)
  - [Lire et écrire](#reading-and-writing)
  - [Clés cpéciales](#special-keys)
  - [Storing Tables](#storing-tables)
  - [Métadonnées privées](#private-metadata)
  - [Lua Tables](#lua-tables)
- [Stockage de mod](#mod-storage)
- [Bases de données](#databases)
- [Décider lequel utiliser](#deciding-which-to-use)
- [À ton tour](#your-turn)

## Métadonnées

### Qu'est-ce que les métadonnées ?

Dans Minetest, les métadonnées sont un magasin de valeurs-clés utilisé pour attacher des données personnalisées à quelque chose.
Vous pouvez utiliser des métadonnées pour stocker des informations sur un nœud, un lecteur ou un ItemStack.

Chaque type de métadonnées utilise exactement la même API.
Les métadonnées stockent les valeurs sous forme de chaînes, mais il existe un certain nombre de méthodes pour
convertir et stocker d'autres types primitifs.

Certaines clés des métadonnées peuvent avoir une signification particulière.
Par exemple, `infotext` dans les métadonnées du nœud est utilisé pour stocker l'info-bulle qui montre
lorsque vous survolez le nœud à l'aide du réticule.
Pour éviter les conflits avec d'autres mods, vous devez utiliser l'espace de noms standard
convention pour les clés: `modname: keyname`.
L'exception concerne les données conventionnelles telles que le nom du propriétaire qui est stocké en tant que
`propriétaire`.

Les métadonnées sont des données sur les données.
Les données elles-mêmes, telles que le type d'un nœud ou le nombre d'une pile, ne sont pas des métadonnées.

### Obtenir les métadonnées d'un objet

Si vous connaissez la position d'un nœud, vous pouvez récupérer ses métadonnées:

```lua
local meta = minetest.get_meta({ x = 1, y = 2, z = 3 })
```

Les métadonnées Player et ItemStack sont obtenues en utilisant `get_meta ()`:

```lua
local pmeta = player:get_meta()
local imeta = stack:get_meta()
```

### Lire et écrire

Dans la plupart des cas, les méthodes `get <type> ()` et `set <type> ()` seront utilisées pour lire
et écris à meta.
Les métadonnées stockent des chaînes, de sorte que les méthodes de chaîne définissent directement et obtiennent la valeur.

```lua
print(meta:get_string("foo")) --> ""
meta:set_string("foo", "bar")
print(meta:get_string("foo")) --> "bar"
```

Tous les getters tapés renverront une valeur par défaut neutre si la clé ne
existent, comme "" "` ou `0 '.
Vous pouvez utiliser `get ()` pour renvoyer une chaîne ou nil.

Les métadonnées étant une référence, toutes les modifications seront automatiquement mises à jour vers la source.
Les ItemStacks ne sont cependant pas des références, vous devrez donc mettre à jour l'itemstack dans le
inventaire.

Les getters et setters non typés seront convertis vers et depuis les chaînes:

```lua
print(meta:get_int("count"))    --> 0
meta:set_int("count", 3)
print(meta:get_int("count"))    --> 3
print(meta:get_string("count")) --> "3"
```

### Clés spéciales

`infotext` est utilisé dans les métadonnées de nœud pour afficher une info-bulle lors du survol du réticule sur un nœud.
Ceci est utile pour montrer la propriété ou le statut d'un nœud.

`description` est utilisé dans les métadonnées ItemStack pour remplacer la description lorsque
survolant la pile dans un inventaire.
Vous pouvez utiliser des couleurs en les encodant avec `minetest.colorize ()`.

`propriétaire` est une clé commune utilisée pour stocker le nom d'utilisateur du joueur qui possède le
élément ou nœud.

### Storing Tables

Les tableaux doivent être convertis en chaînes avant de pouvoir être stockés.
Minetest propose deux formats pour ce faire: Lua et JSON.

La méthode Lua a tendance à être beaucoup plus rapide et correspond au format Lua
utilise pour les tableaux, alors que JSON est un format plus standard, est mieux
structuré et est bien adapté lorsque vous avez besoin d'échanger des informations
avec un autre programme.


```lua
local data = { username = "player1", score = 1234 }
meta:set_string("foo", minetest.serialize(data))

data = minetest.deserialize(minetest:get_string("foo"))
```

### Métadonnées privées

Les entrées dans les métadonnées de nœud peuvent être marquées comme privées et non envoyées au client.
Les inscriptions non marquées comme privées seront envoyées au client.

```lua
meta:set_string("secret", "asd34dn")
meta:mark_as_private("secret")
```

### Lua Tables

Vous pouvez convertir vers et depuis les tables Lua en utilisant `to_table` et` from_table`:
```lua
local tmp = meta:to_table()
tmp.foo = "bar"
meta:from_table(tmp)
```

## Stockage de mod

Le stockage de mod utilise exactement la même API que les métadonnées, bien que ce ne soit pas techniquement
Métadonnées.
Le stockage de mod est par mod, et ne peut être obtenu que pendant le temps de chargement afin de
savoir quel mod le demande.


```lua
local storage = minetest.get_mod_storage()
```

Vous pouvez maintenant manipuler le stockage comme les métadonnées:
```lua
storage:set_string("foo", "bar")
```

## Bases de données

Si le mod est susceptible d'être utilisé sur un serveur et stockera de nombreuses données,
c'est une bonne idée d'offrir une méthode de stockage de base de données.
Vous devez rendre cela facultatif en séparant comment les données sont stockées et où
c'est utilisé.


```lua
local backend
if use_database then
    backend =
        dofile(minetest.get_modpath("mymod") .. "/backend_sqlite.lua")
else
    backend =
        dofile(minetest.get_modpath("mymod") .. "/backend_storage.lua")
end

backend.get_foo("a")
backend.set_foo("a", { score = 3 })
```

Le fichier backend_storage.lua doit inclure une implémentation de stockage de mod:
```lua
local storage = minetest.get_mod_storage()
local backend = {}

function backend.set_foo(key, value)
    storage:set_string(key, minetest.serialize(value))
end

function backend.get_foo(key, value)
    return minetest.deserialize(storage:get_string(key))
end

return backend
```

Le backend_sqlite ferait une chose similaire, mais utiliser la bibliothèque Lua * lsqlite3 *
au lieu du stockage de mod.

L'utilisation d'une base de données telle que SQLite nécessite l'utilisation d'un environnement non sécurisé.
Un environnement non sécurisé est une table qui n'est disponible que pour les mods
explicitement sur liste blanche par l'utilisateur, et il contient un moins restreint
copie de l'API Lua qui pourrait être utilisée abusivement si elle était disponible pour les mods malveillants.
Les environnements non sécurisés seront traités plus en détail dans le
Chapitre [Sécurité](../qualité/security.html).

```lua
local ie = minetest.request_insecure_environment()
assert(ie, "Please add mymod to secure.trusted_mods in the settings")

local _sql = ie.require("lsqlite3")
-- Prevent other mods from using the global sqlite3 library
if sqlite3 then
    sqlite3 = nil
end
```

L'enseignement de SQL ou de l'utilisation de la bibliothèque lsqlite3 est hors de portée pour ce livre.

## Décider lequel utiliser

Le type de méthode que vous utilisez dépend de l'objet des données,
comment il est formaté et quelle est sa taille.
À titre indicatif, les petites données peuvent atteindre 10 000 Ko, les données moyennes jusqu'à 10 Mo et les grandes
les données sont de n'importe quelle taille au-dessus de cela.

Les métadonnées de nœud sont un bon choix lorsque vous devez stocker des données liées à un nœud.
Le stockage de données moyennes est assez efficace si vous les rendez privées.

Les métadonnées d'élément ne doivent pas être utilisées pour stocker autre chose que de petites quantités de données car elles ne le sont pas.
possible d'éviter de l'envoyer au client.
Les données seront également copiées chaque fois que la pile est déplacée ou accessible depuis Lua.

Le stockage de mod est bon pour les données moyennes, mais l'écriture de grandes données peut être inefficace.
Il est préférable d'utiliser une base de données pour les données volumineuses pour éviter d'avoir à écrire toutes les
des données à chaque sauvegarde.

Les bases de données ne sont viables que pour les serveurs en raison de la
besoin de mettre le mod sur liste blanche pour accéder à un environnement non sécurisé.
Ils sont bien adaptés aux grands ensembles de données.

## À ton tour

* Créez un nœud qui disparaît après avoir été perforé cinq fois.
(Utilisez `on_punch` dans la définition du nœud et` minetest.set_node`.)

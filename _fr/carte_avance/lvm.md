---
title: Manipulateurs Lua Voxel
layout: default
root: ../..
idx: 6.1
description: Apprenez à utiliser les LVM pour accélérer les opérations cartographiques.
redirect_from:
  - /en/chapters/lvm.html
  - /en/map/lvm.html
mapgen_object:
    level: warning
    title: LVMs and Mapgen
    message: Don't use `minetest.get_voxel_manip()` with mapgen, as it can cause glitches.
            Use `minetest.get_mapgen_object("voxelmanip")` instead.
---

## Introduction <!-- omit in toc -->

Les fonctions décrites dans le chapitre [Opérations de base sur les cartes](environment.html)
sont pratiques et faciles à utiliser, mais pour de grandes surfaces, ils sont inefficaces.
Chaque fois que vous appelez `set_node` ou` get_node`, votre mod doit communiquer avec
le moteur. Il en résulte des opérations de copie individuelles constantes entre les
moteur et votre mod, qui est lent et diminuera rapidement les performances de
ton jeu. L'utilisation d'un manipulateur Lua Voxel (LVM) peut être une meilleure alternative.

- [Conceptes](#concepts)
- [Lire dans le LVM](#reading-into-the-lvm)
- [Lecture des nœuds](#reading-nodes)
- [Écriture de nœuds](#writing-nodes)
- [Exemple](#example)
- [À ton tour](#your-turn)

## Conceptes

Un LVM vous permet de charger de grandes zones de la carte dans la mémoire de votre mod.
Vous pouvez ensuite lire et écrire ces données sans autre interaction avec le
moteur et sans exécuter de rappels, ce qui signifie que ces
les opérations sont très rapides. Une fois terminé, vous pouvez ensuite réécrire la zone dans
le moteur et exécutez tous les calculs d'éclairage.

## Lire dans le LVM

Vous ne pouvez charger qu'une zone cubique dans un LVM, vous devez donc déterminer le minimum
et les positions maximales que vous devez modifier. Ensuite, vous pouvez créer et lire
un LVM. Par exemple:

```lua
local vm         = minetest.get_voxel_manip()
local emin, emax = vm:read_from_map(pos1, pos2)
```

Pour des raisons de performances, un LVM ne lira presque jamais la zone exacte à laquelle vous le dites.
Au lieu de cela, il lira probablement une zone plus grande. La plus grande surface est donnée par `emin` et` emax`,
qui représentent *émergé min pos* et *émergé max pos*. Un LVM chargera la zone
qu'il contient pour vous - qu'il s'agisse de charger à partir de la mémoire, du disque ou
appeler le générateur de carte.

{% include notice.html notice=page.mapgen_object %}

## Lecture des nœuds

Pour lire les types de nœuds à des positions particulières, vous devrez utiliser `get_data ()`.
Cela renvoie un tableau plat où chaque entrée représente le type d'un
nœud particulier.

```lua
local data = vm:get_data()
```

You can get param2 and lighting data using the methods `get_light_data()` and `get_param2_data()`.

You'll need to use `emin` and `emax` to work out where a node is in the flat arrays
given by the above methods. There's a helper class called `VoxelArea` which handles
the calculation for you.

```lua
local a = VoxelArea:new{
    MinEdge = emin,
    MaxEdge = emax
}

-- Get node's index
local idx = a:index(x, y, z)

-- Read node
print(data[idx])
```

Lorsque vous exécutez ceci, vous remarquerez que `data [vi]` est un entier. Ceci est dû au fait
le moteur ne stocke pas les nœuds à l'aide de chaînes, pour des raisons de performances.
Au lieu de cela, le moteur utilise un entier appelé ID de contenu.
Vous pouvez trouver l'ID de contenu pour un type particulier de nœud avec
`get_content_id ()`. Par exemple:

```lua
local c_stone = minetest.get_content_id("default:stone")
```

Vous pouvez ensuite vérifier si le nœud est en pierre:

```lua
local idx = a:index(x, y, z)
if data[idx] == c_stone then
    print("is stone!")
end
```

Il est recommandé de rechercher et de stocker les ID de contenu des types de nœuds
au moment du chargement, car les ID d'un type de nœud ne changeront jamais. Assurez-vous de stocker
les ID dans une variable locale pour des raisons de performances.

Les nœuds d'un tableau de données LVM sont stockés dans l'ordre inverse des coordonnées, vous devez donc
itérez toujours dans l'ordre «z, y, x». Par exemple:

```lua
for z = min.z, max.z do
    for y = min.y, max.y do
        for x = min.x, max.x do
            -- vi, voxel index, is a common variable name here
            local vi = a:index(x, y, z)
            if data[vi] == c_stone then
                print("is stone!")
            end
        end
    end
end
```

La raison de cela touche le sujet de l'architecture informatique. La lecture depuis la RAM est plutôt
coûteux, les processeurs ont donc plusieurs niveaux de mise en cache. Si les données qu'un processus demande
est dans le cache, il peut le récupérer très rapidement. Si les données ne sont pas dans le cache,
puis un échec de cache se produit et il va récupérer les données dont il a besoin dans la RAM. Toutes les données
entourant les données demandées est également récupéré puis remplace les données dans le cache. C'est
car il est fort probable que le processus demande à nouveau des données à proximité de cet emplacement. Ça signifie
une bonne règle d'optimisation est d'itérer de manière à lire les données l'une après l'autre
un autre, et évitez le *cache thrashing*.

## Écriture de nœuds

First, you need to set the new content ID in the data array:

```lua
for z = min.z, max.z do
    for y = min.y, max.y do
        for x = min.x, max.x do
            local vi = a:index(x, y, z)
            if data[vi] == c_stone then
                data[vi] = c_air
            end
        end
    end
end
```

Lorsque vous avez terminé de définir les nœuds dans LVM, vous devez ensuite télécharger les données
tableau au moteur:

```lua
vm:set_data(data)
vm:write_to_map(true)
```

Pour définir l'éclairage et les données param2, utilisez le nom approprié
Méthodes `set_light_data ()` et `set_param2_data ()`.

`write_to_map ()` prend un booléen qui est vrai si vous voulez que l'éclairage soit
calculé. Si vous passez faux, vous devez recalculer l'éclairage à un avenir
en utilisant `minetest.fix_light`.

## Exemple

```lua
-- Get content IDs during load time, and store into a local
local c_dirt  = minetest.get_content_id("default:dirt")
local c_grass = minetest.get_content_id("default:dirt_with_grass")

local function grass_to_dirt(pos1, pos2)
    -- Read data into LVM
    local vm = minetest.get_voxel_manip()
    local emin, emax = vm:read_from_map(pos1, pos2)
    local a = VoxelArea:new{
        MinEdge = emin,
        MaxEdge = emax
    }
    local data = vm:get_data()

    -- Modify data
    for z = pos1.z, pos2.z do
        for y = pos1.y, pos2.y do
            for x = pos1.x, pos2.x do
                local vi = a:index(x, y, z)
                if data[vi] == c_grass then
                    data[vi] = c_dirt
                end
            end
        end
    end

    -- Write data
    vm:set_data(data)
    vm:write_to_map(true)
end
```

## À ton tour

* Créer `replace_in_area (from, to, pos1, pos2)`, qui remplace toutes les instances de
   `from` avec` to` dans la zone donnée, où `from` et` to` sont des noms de nœuds.
* Faire une fonction qui fait rotates all chest nodes by 90&deg;.
* Créez une fonction qui utilise un LVM pour propager le pavé moussu aux environs
   nœuds de pierre et de galets.
   Votre implémentation fait-elle que le pavé moussu se propage sur plus d'une distance d'un nœud chacun
   temps? Si oui, comment pouvez-vous arrêter cela?

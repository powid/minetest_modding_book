---
title: Introduction aux architectures propres
layout: default
root: ../..
idx: 8.4
---

## Introduction <!-- omit in toc -->

Une fois que votre mod atteint une taille respectable, vous le trouverez de plus en plus difficile à
Gardez le code propre et exempt de bogues. C'est un problème particulièrement important lors de l'utilisation
un langage typé dynamiquement comme Lua, étant donné que le compilateur vous donne très peu
aide au moment de la compilation quand il s'agit de vérifier que les types sont utilisés correctement.

Ce chapitre couvre les concepts importants nécessaires pour garder votre code propre,
et des modèles de conception communs pour y parvenir. Veuillez noter que ce chapitre n'est pas
censé être normatif, mais plutôt vous donner une idée des possibilités.
Il n'y a pas de bonne façon de concevoir un mod, et une bonne conception de mod est très subjective.


- [Cohésion, couplage et séparation des préoccupations](#cohesion-coupling-and-separation-of-concerns)
- [Observateur](#observer)
- [Modèle Vue Contrôleur](#model-view-controller)
  - [API-View](#api-view)
- [Conclusion](#conclusion)


## Cohésion, couplage et séparation des préoccupations

Sans aucune planification, un projet de programmation aura tendance à
code de spaghetti. Le code spaghetti se caractérise par un manque de structure - tous les
le code est jeté ensemble sans limites claires. Cela fait finalement un
projet complètement irréalisable, aboutissant à son abandon.

L’opposé de ceci est de concevoir votre projet comme une collection de
petits programmes ou zones de code. <!-- Weird wording? -->

> Inside every large program, there is a small program trying to get out.
>
>  --C.A.R. Hoare

Cela devrait être fait de manière à ce que vous réalisiez la séparation des préoccupations -
chaque domaine doit être distinct et répondre à un besoin ou à une préoccupation distinct.

Ces programmes / domaines doivent avoir les deux propriétés suivantes:

* **Haute cohésion** - la zone doit être étroitement / étroitement liée.
* **Couplage bas** - gardez les dépendances entre les zones aussi basses que possible et évitez
s'appuyant sur des implémentations internes. C'est une très bonne idée de vous assurer d'avoir
une faible quantité de couplage, car cela signifie que la modification des API de certaines zones
sera plus réalisable.

Notez que ceux-ci s'appliquent à la fois lorsque vous pensez à la relation entre les mods,
et la relation entre les zones à l'intérieur d'un mod.


## Observateur

Un moyen simple de séparer les différentes zones de code consiste à utiliser le modèle Observer.

Prenons l'exemple du déverrouillage d'un exploit lorsqu'un joueur tue un
animal rare. L'approche naïve serait d'avoir un code de réussite dans la foule
tuer la fonction, vérifier le nom de la foule et déverrouiller le prix s'il correspond.
C'est une mauvaise idée, cependant, car cela rend le mod mobs couplé aux réalisations
code. Si vous continuez à faire cela - par exemple, en ajoutant de l'XP au code de mort de la foule -
vous pourriez vous retrouver avec beaucoup de dépendances désordonnées.

Entrez le modèle Observateur. Au lieu que le mod mymobs se soucie des récompenses,
le mod mymobs expose un moyen pour d'autres zones de code d'enregistrer leur
intérêt pour un événement et recevoir des données sur l'événement.

```lua
mymobs.registered_on_death = {}
function mymobs.register_on_death(func)
    table.insert(mymobs.registered_on_death, func)
end

-- in mob death code
for i=1, #mymobs.registered_on_death do
    mymobs.registered_on_death[i](entity, reason)
end
```

Ensuite, l'autre code enregistre son intérêt:

```lua
mymobs.register_on_death(function(mob, reason)
    if reason.type == "punch" and reason.object and
            reason.object:is_player() then
        awards.notify_mob_kill(reason.object, mob.name)
    end
end)
```

Vous pensez peut-être - attendez une seconde, cela semble terriblement familier. Et tu as raison!
L'API Minetest est fortement basée sur Observer pour empêcher le moteur de se soucier de
ce qui écoute quelque chose.


## Modèle Vue Contrôleur

Dans le chapitre suivant, nous verrons comment tester automatiquement votre
code et l'un des problèmes que nous aurons est de savoir comment séparer votre logique
(calculs, ce qui devrait être fait) à partir des appels d'API (`minetest. *`, autres mods)
autant que possible.

Une façon de le faire est de penser à:

* Quelles **données** vous avez.
* Quelles **actions** vous pouvez entreprendre avec ces données.
* Comment **les événements** (c.-à-d.: Formspec, poinçons, etc.) déclenchent ces actions, et comment
  ces actions provoquent des événements dans le moteur.

Prenons un exemple de mod de protection des terres. Les données dont vous disposez sont les zones
et toutes les métadonnées associées. Les actions que vous pouvez entreprendre sont «créer», «modifier» ou
`supprimer`. Les événements qui déclenchent ces actions sont les commandes de discussion et formspec
recevoir des champs. Ce sont 3 domaines qui peuvent généralement être assez bien séparés.

Dans vos tests, vous serez en mesure de vous assurer qu’une action déclenchée ne
la bonne chose aux données. Vous n'aurez pas besoin de tester qu'un événement appelle un
action (car cela nécessiterait l'utilisation de l'API Minetest et de cette zone de code
devrait être aussi petit que possible de toute façon.)

Vous devez écrire votre représentation de données en utilisant Pure Lua. "Pure" dans ce contexte
signifie que les fonctions peuvent s'exécuter en dehors de Minetest - aucun des moteurs
les fonctions sont appelées.

```lua
-- Data
function land.create(name, area_name)
    land.lands[area_name] = {
        name  = area_name,
        owner = name,
        -- more stuff
    }
end

function land.get_by_name(area_name)
    return land.lands[area_name]
end
```

Vos actions doivent également être pures, mais appeler d'autres fonctions est plus
acceptable que dans ce qui précède.

```lua
-- Controller
function land.handle_create_submit(name, area_name)
    -- process stuff
    -- (ie: check for overlaps, check quotas, check permissions)

    land.create(name, area_name)
end

function land.handle_creation_request(name)
    -- This is a bad example, as explained later
    land.show_create_formspec(name)
end
```

Vos gestionnaires d'événements devront interagir avec l'API Minetest. Tu devrais garder
le nombre de calculs au minimum, car vous ne pourrez pas tester cette zone
très facilement.

```lua
-- View
function land.show_create_formspec(name)
    -- Note how there's no complex calculations here!
    return [[
        size[4,3]
        label[1,0;This is an example]
        field[0,1;3,1;area_name;]
        button_exit[0,2;1,1;exit;Exit]
    ]]
end

minetest.register_chatcommand("/land", {
    privs = { land = true },
    func = function(name)
        land.handle_creation_request(name)
    end,
})

minetest.register_on_player_receive_fields(function(player,
            formname, fields)
    land.handle_create_submit(player:get_player_name(),
            fields.area_name)
end)
```
Ce qui précède est le modèle Model-View-Controller. Le modèle est une collection de données
avec des fonctions minimales. La vue est un ensemble de fonctions qui écoutent
événements et le transmettre au contrôleur, et reçoit également des appels du contrôleur à
faire quelque chose avec l'API Minetest. Le contrôleur est l'endroit où les décisions et
la plupart des calculs sont effectués.

Le contrôleur ne doit avoir aucune connaissance de l'API Minetest - remarquez comment
il n'y a aucun appel Minetest ni aucune fonction de vue qui leur ressemble.
Vous ne devriez *PAS* avoir une fonction comme `view.hud_add (player, def)`.
Au lieu de cela, la vue définit certaines actions que le contrôleur peut indiquer à la vue,
comme `view.add_hud (info)` où info est une valeur ou une table qui ne se rapporte pas
à l'API Minetest du tout.

<figure class="right_image">
    <img
        width="100%"
        src="{{ page.root }}/static/mvc_diagram.svg"
        alt="Diagram showing a centered text element">
</figure>

Il est important que chaque zone ne communique qu'avec ses voisins directs,
comme indiqué ci-dessus, afin de réduire le nombre de modifications à apporter en cas de modification
internes ou externes d'un domaine. Par exemple, pour modifier la spécification de formulaire que vous
aurait seulement besoin de modifier la vue. Pour modifier l'API de vue, il vous suffit de
changer la vue et le contrôleur, mais pas le modèle du tout.

En pratique, cette conception est rarement utilisée en raison de la complexité accrue
et parce qu'il ne donne pas beaucoup d'avantages pour la plupart des types de mods. Au lieu,
vous verrez généralement un type de conception moins formel et strict -
variantes de l'API-View.


### API-View

Dans un monde idéal, vous auriez les 3 zones ci-dessus parfaitement séparées avec toutes
les événements entrant dans le contrôleur avant de revenir à la vue normale. Mais
ce n'est pas le monde réel. Un bon compromis est de réduire le mod en deux
les pièces:

* **API** - C'était le modèle et le contrôleur ci-dessus. Il ne devrait y avoir aucune utilisation de
    `minetest.` ici.
* **Vue** - C'était également la vue ci-dessus. C'est une bonne idée de structurer cela en
    fichiers pour chaque type d'événement.

rubenwardy [mod d'artisanat](https://github.com/rubenwardy/crafting) à peu près
suit cette conception. `api.lua` est presque toutes les fonctions Lua pures qui traitent les données
stockage et calculs de type contrôleur. `gui.lua` est la vue des formulaires
et la soumission formspec, et «async_crafter.lua» est la vue et le contrôleur pour
un nœud formspec et des temporisateurs de nœud.

Séparer le mod comme ceci signifie que vous pouvez très facilement tester la partie API,
car il n'utilise aucune API Minetest - comme indiqué dans le
[chapitre suivant](unit_testing.html) et vu dans le mod d'artisanat.


## Conclusion

Une bonne conception de code est subjective et dépend fortement du projet que vous réalisez. Comme un
règle générale, essayez de maintenir une cohésion élevée et un couplage faible. Formulé différemment,
garder le code associé ensemble et le code non lié à part, et garder les dépendances simples.

Je recommande fortement de lire les [Modèles de programmation de jeu](http://gameprogrammingpatterns.com/)
livre. Il est disponible gratuitement pour [lire en ligne](http://gameprogrammingpatterns.com/contents.html)
et va beaucoup plus en détail sur les modèles de programmation communs pertinents pour les jeux.

---
title: Blocks, Objets et Fabriquer
layout: default
root: ../..
idx: 2.1
description: Apprendre comment enregistrer un block, des objetsbe et des recettes de fabrication en utilisant register_node, register_item et register_craft.
redirect_from: /fr/chapters/block_objets_fabriquer.html
---

## Introduction <!-- omit in toc -->

Enregistrer des nouveaux blocks et objets à fabriquer ainsi que créer les recettes de fabrications sont les bases requises pour beaucoup de mods.

- [Que sont les Blocks et les Objets ?](#que-sont-les-blocks-et-les-objets)
- [Enregistrer des Objets](#enregistrer-des-objets)
  - [Noms et Alias des Objets](#noms-et-alias-des-objets)
  - [Textures](#textures)
- [Enregistrer un block de base](#enregistrer-un-block-de-base)
- [Actions et Événements](#actions-et-événements)
  - [on_use](#onuse)
- [Fabriquer](#fabriquer)
  - [Shaped](#shaped)
  - [Shapeless](#shapeless)
  - [Cooking et Fuel](#cooking-et-fuel)
- [Groupes](#groupes)
- [Outils, Capacités et Types de Creusés](#outils-capacités-et-types-de-creusés)

## Que sont les Blocks et les Objets ?

Les blocks, les objets manufacturés et les outils sont tous des Objets.
Un objet est quelque chose qui peut être trouvé dans l'inventaire –
Même dans le cas où c'est impossible dans un gameplay normal.

Un block est un objet qui peut être placé ou trouvé dans le monde.
Chaque coordonées [X,Y,Z] du monde doit être occupée par un et un seul block –
Les blocks visuellements vides sont des blocks d'air.

Un objet manufacturé ("craftitem") ne peut être placé et ne peut être trouvé que dans l'inventaire ou en tant qu'objet-butin dans le monde.

Un outil peut être porté et a en général des capacités de creuser particulières.
Dans le future, il semble que les objets manufacturés et les outils soient fusionnés en un seul type d'objet,
étant donné que la différence entre eux est plutôt artificielle.

## Enregistrer des Objets

La définitions des Objets réside en *item name* (ie : nom de l'objet) et une *definition table* (ie : Tableau de définition)
Le tableau de définition contient les attributs qui affectent les comportements d'un objets.

```lua
minetest.register_craftitem("modname:itemname", {
    description = "Mon objet spécial",
    inventory_image = "modname_itemname.png"
})
```

### Noms et Alias des Objets

Tout objet a un nom d'objet utilisé pour y faire référence, il sera au format suivant :

    modname:itemname

modname est le nom du mod dans lequel l'objet est enregistré et itemname est le nom de l'objet lui-même.
Le nom de l'objet doit être pertinent quant à ce qu'est l'objet et ne doit pas être déjà enregistré.

Les objets peuvent également avoir des *alias* pointant vers leur nom.
Un *alias* est un pseudo nom d'objet qui amène le moteur à traiter toute
occurence de l'alias comme s'il s'agissait du nom de l'objet.
Il y a deux manières communes de les utiliser :

* Renommer un objet supprimé en quelque chose d'autre.
  Il peut y avoir des unknown nodes (ie : blocks inconnus) dans le monde et dans l'inventaire
  si un objet est supprimé d'un mod, sans code correctif.
  Il est important d'éviter d'attribuer un alias vers un block impossible à obtenir
  si le block supprimé peut être otenu.
* Ajouter un raccourci. `/giveme dirt` (= "donne-moi terre") est plus facile que `/giveme default:dirt` (= "donne-moi terre issue du mod default").

```lua
minetest.register_alias("dirt", "default:dirt")
```

Les mods doivent s'assurer de résoudre les alias avant de traiter les différents noms d'objets,
étant donné que le moteur ne le fera pas.
C'est plutôt simple cependant :

```lua
itemname = minetest.registered_aliases[itemname] or itemname
```

### Textures

Les textures doivent être placée dans le dossier textures/ avec le nom dans le format `modname_itemname.png`.\\
Les textures JPEG sont supportée mais elles ne permettent pas la transparence et sont généralement de mauvaise qualité dans de faibles résolutions.
Il est souvent mieux d'opter pour le format PNG.

Les texrtues dans Minetest sont habituellement 16×16 pixels.
Elles peuvent être de n'importe quelle résolution, mais il est recommandé qu'elle soient une puissance de 2,
par exemple : 16, 32, 64 ou 128.
D'autres résolutions pourraient ne pas être supportées correctement sur de vieux appareils,
résultant vers une baisse des performences.


## Enregistrer un block basique

```lua
minetest.register_node("mymod:diamond", {
    description = "Alien Diamond",
    tiles = {"mymod_diamond.png"},
    is_ground_content = true,
    groups = {cracky=3, stone=1}
})
```

La propriété `tiles` est une table de noms de texture que le block va utiliser.
Quand il y a une seule texture, cette texture sera utilisée sur chaque côté.
Pour donner une texture différente à chaque côté, mettez les 6 noms de textures dans cet ordre :

    haut (+Y), bas (-Y), droite (+X), gauche (-X), dos (+Z), face (-Z).
    (+Y, -Y, +X, -X, +Z, -Z)

Rappelez-vous que dans Minetest, +Y est montant comme c'est la convention en infographie 3D.

```lua
minetest.register_node("mymod:diamond", {
    description = "Alien Diamond",
    tiles = {
        "mymod_diamond_up.png",    -- y+
        "mymod_diamond_down.png",  -- y-
        "mymod_diamond_right.png", -- x+
        "mymod_diamond_left.png",  -- x-
        "mymod_diamond_back.png",  -- z+
        "mymod_diamond_front.png", -- z-
    },
    is_ground_content = true,
    groups = {cracky = 3},
    drop = "mymod:diamond_fragments"
    -- ^  Rather than dropping diamond, drop mymod:diamond_fragments
})
```

L'attribut is_ground_content permet aux grottes d'êtres générées sur les pierres.
C'est essentiel pour tout block qui peut être placé en sous-sol pendant la génération de la carte.
Les grottes sont creusées dans le monde après que tous les autres blocks de la zone ait été générés.

## Actions et Événements

Minetest utilise grandement un design événementiel (ie : Callbacks).
Les événements peuvent être placés dans la tableau de définition d'un objet pour permettre
des réponses aux différents événements utilisateurs.

### on_use

Par défaut, l'événement `use` est déclenché quand le joueur fait un clic gauche avec un objet.
Avoir un événement rattaché à `use` empêche cet objet d'être utilisé pour creuser des blocks.
L'une des utilisations communes du déclencheur `use` est pour la nourriture :

```lua
minetest.register_craftitem("mymod:mudpie", {
    description = "Alien Mud Pie",
    inventory_image = "myfood_mudpie.png",
    on_use = minetest.item_eat(20),
})
```

Le nombre donné à la fonction `minetest.item_eat` est le nombre de points de vie redonnés
quand cette nourriture est consommée.
Chaque icône cœur que le joueur a vaut deux points de vie.
Un joueur peut habituellement avoir jusqu'à 10 cœurs, ce qui vaut 20 points de vie.
Les points de vies ne sont pas nécessairement entiers, ils peuvent être décimaux.

`minetest.item_eat()` est une fonction qui retourne une fonction, 
l'affectant à l'événement `on_use`.
Cela signifie que le code ci-dessus est similaire à ça :

```lua
minetest.register_craftitem("mymod:mudpie", {
    description = "Alien Mud Pie",
    inventory_image = "myfood_mudpie.png",
    on_use = function(...)
        return minetest.do_item_eat(20, nil, ...)
    end,
})
```

Comprendre comment `item_eat` fonctionne en retournant simplement une fonction
vous permettra de le modifier pour des objectifs plus complexes comme jouer un son particulier.

## Fabriquer

Il y a différents types de recettes de fabrication disponibles, indiquées par la propriété `type`.

* shaped - Les ingrédients doivent être aux bons emplacements.
* shapeless - Les ingrédients peuvent avoir n'importe quels emplacements.
  Ils doivent simplement être en bonnes quantités.
* cooking - Recettes utilisées par le four.
* fuel - Définie un objet qui peut être utilisé comme carburant dans les fours.
* tool_repair - Définie les objets qui peuvent être réparés par des outils.

Les recettes de fabriquation ne sont pas des objets, elles n'utilisent donc pas de Noms d'Objets pour les identifiées.

### Shaped

Les recettes « shaped » (avec une figure) sont celles quand les ingrédients doivent être dans la bonne figure ou modèle pour fonctionner.
Dans l'exemple ci-dessous, les fragments doivent être placés en forme de chaise pour que la fabrication fonctionne.

```lua
minetest.register_craft({
    type = "shaped",
    output = "mymod:diamond_chair 99",
    recipe = {
        {"mymod:diamond_fragments", "",                         ""},
        {"mymod:diamond_fragments", "mymod:diamond_fragments",  ""},
        {"mymod:diamond_fragments", "mymod:diamond_fragments",  ""}
    }
})
```

Une chose à noter est la colonne blanche dans la partie droite.
Cela veut dire qu'il *doit* y avoir une colonne vide sur la droite de la figure, sinon,
cela ne fonctionnera pas.
Si une colonne vide n'est pas requise, alors les strings vides peut être retirées, comme suit :

```lua
minetest.register_craft({
    output = "mymod:diamond_chair 99",
    recipe = {
        {"mymod:diamond_fragments", ""                       },
        {"mymod:diamond_fragments", "mymod:diamond_fragments"},
        {"mymod:diamond_fragments", "mymod:diamond_fragments"}
    }
})
```

Le champ `type` n'est pas nécessaire pour les recettes avec une figure, étant donné que
`shaped` est le type par défaut.

### Shapeless

Les recettes « shapeless » sont un type de recette qui sont utilisées quand l'ordre
dans lequel les ingrédients sont placés n'importe pas, simplement qu'ils soient présents.

```lua
minetest.register_craft({
    type = "shapeless",
    output = "mymod:diamond 3",
    recipe = {
        "mymod:diamond_fragments",
        "mymod:diamond_fragments",
        "mymod:diamond_fragments",
    },
})
```

### Cooking et Fuel

Les recettes avec le type « cooking » ne sont pas faites dans la grille de fabrication,
mais cuites dans des fours ou d'autres outils de cuisson qui peuvent être trouvés dans des mods.

```lua
minetest.register_craft({
    type = "cooking",
    output = "mymod:diamond_fragments",
    recipe = "default:coalblock",
    cooktime = 10,
})
```

La seule différence réelle dans le code est que la recette n'a qu'un seul objet,
comparée à être en tableau (entre accolade `{}`).
Il y a aussi un paramètre optionnel « cooktime » qui définie le temps de cuisson que
mettra l'objet.
S'il n'est pas définie, il vaut 3 par défaut.
L'unité utilisée est la seconde.

La recette ci-dessus fonctionne lorsque qu'un block de charbon est en entrée,
peu importe le carburant en dessous.
Cela créer un fragment de diamand après 10 secondes !

Le type `fuel` accompagne le type `cooking`.
Il définie ce qui peut être utilisé comme carburant dans le four et autres outils de cuisson issus de mods.

```lua
minetest.register_craft({
    type = "fuel",
    recipe = "mymod:diamond",
    burntime = 300,
})
```

Ces recettes n'ont pas de sortie contrairement aux autres recettes, mais elles ont un temps de combustion `burntime`,
l'unité est la seconde.
Donc le diamant est un bon carburant pour 300 secondes !

## Groupes

Les objets peuvent être membres de plein de groupes et les groupes peuvent avoir plein de membres.
Les groupes sont définis en utilisant la propriété `groups` dans le tableau de définition et ont une valeur associée.

```lua
groups = {cracky = 3, wood = 1}
```

Il y a plusieurs raisons pour lesquelles utiliser les groupes.
Premièrement, les groupes permettent de décrire les propriété comme le type de creuse et l'inflamabilité.
Secondement, les groupes peuvent être utilisés dans des recettes de fabrication plutôt que des noms d'objet pour
permettre d'utiliser n'importe quel objet du groupe.

```lua
minetest.register_craft({
    type = "shapeless",
    output = "mymod:diamond_thing 3",
    recipe = {"group:wood", "mymod:diamond"}
})
```

## Outils, Capacités et Types de Creusés

Les types de creusés (dig types) sont des groupes qui sont utilisés pour définir la force de forage d’un block lorsqu’il est creusé avec différents outils.
Un groupe de type de creusés avec une grande valeur associée signifie que le block est plus facile et rapide à creuser.
Il est possible de combiner plusieurs type de creusés pour permettre à plusieurs types d’outils d'être efficaces dessus.
Un block sans type de creusé ne peut être creusé avec aucun outil.


| Groupe  | Meilleur outil | Description |
|--------|-----------|-------------|
| crumbly | pelle    | Terre, sable. |
| cracky | pioche   | Des objets durs (mais cassants) comme la pierre. |
| snappy | *any*       | Peut être cassé à l’aide d’outils fins;<br>ex : feuilles, petits plants, câbles, feuilles de métal leaves,…  |
| choppy | hache | Peut être coupé en utilisant une force tranchante;<br>ex : arbres, planches de bois. |
| fleshy | épée | Choses vivantes comme les animaux et les joueurs.<br>Peut impliquer des effets de sang quand frappé. |
| explody | ? | Particulièrement sujets à explosion.  |
| oddly_breakable_by_hand | *any* | Cassable à la main, rapide à creuser;<br>ex : Torches et autres |


Tout outil a nue capacité d’outil (tool capability).
Une capacité inclue une liste de types de creusés supportés and des propriétés associées pour chaque type comme le temps de creuser ainsi que l’usure.
Les outils peuvent aussi avoir une dureté maximum pour chaque type, ce qui empêche les outils plus faibles de creuser des blocks durs.
Il est très commun que les outils incluent les différents types dans leurs capacités,
les moins appropriés ayant des propriétés très inefficaces.
Si l’objet utilisé par le joueur n’a pas une capacité d’outil explicitement attitrée, alors la capacité de la main courrante est utilisée à la place.

```lua
minetest.register_tool("mymod:tool", {
    description = "My Tool",
    inventory_image = "mymod_tool.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            crumbly = {
                maxlevel = 2,
                uses = 20,
                times = { [1]=1.60, [2]=1.20, [3]=0.80 }
            },
        },
        damage_groups = {fleshy=2},
    },
})
```

Groupcaps est la liste des différents types de creusés supporter pour creuser les blocks.
Damage groups permet de contrôler comment l’outil endommage les objets, ça fera l’objet d’une discution ultérieure dans le chapitre Objets, Joueurs et Entités.

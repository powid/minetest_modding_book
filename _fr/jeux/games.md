---
title: Création de jeux
layout: default
root: ../..
idx: 7.1
---

## Introduction <!-- omit in toc -->

La force de Minetest est la facilité avec laquelle on peut développer des jeux sans avoir besoin
de créer vos propres graphiques voxel, algorithmes voxel ou code réseau sophistiqué.

- [Qu'est-ce qu'un jeu ?](#what-is-a-game)
- [Le dossier « Games »](#game-directory)
- [Compatibilités des jeux](#inter-game-compatibility)
	- [Compatibilité API](#api-compatibility)
	- [Groupes and Alias](#groups-and-aliases)
- [À ton tour !](#your-turn)

## Qu'est-ce qu'un jeu ?

Les jeux sont une collection de mods qui fonctionnent ensemble pour créer un jeu cohérent.
Un bon jeu a un thème sous-jacent cohérent et une direction. Par exemple,
il pourrait s'agir d'un jeu de type « minage » classique mais avec des éléments de survie difficiles, ou
ce pourrait être un jeu de simulation spatiale avec une esthétique d'automatisation steampunk.

La conception de jeux est un sujet complexe et constitue en fait tout un domaine d'expertise.
Il est au-delà de la portée du livre de le toucher plus que brièvement.

## Le Dossier « Games »

La structure et l'emplacement d'un jeu vous sembleront assez familiers après avoir travaillé
avec des mods.
Les jeux se trouvent dans un emplacement de jeu, tel que `minetest/games/foo_game`.

	foo_game
	├── game.conf
	├── menu
	│   ├── header.png
	│   ├── background.png
	│   └── icon.png
	├── minetest.conf
	├── mods
	│   └── ... mods
	├── README.txt
	└── settingtypes.txt

La seule chose requise est un dossier mods, mais `game.conf` et` menu / icon.png`
sont recommandés.

## Compatibilité entre les jeux

### Compatibilité API

C'est une bonne idée d'essayer de garder autant de compatibilité API avec Minetest Game que
pratique, car cela rendra le portage des mods vers un autre jeu beaucoup plus facile.

La meilleure façon de conserver la compatibilité avec un autre jeu est de conserver la compatibilité des API
avec tous les mods qui ont le même nom.
Autrement dit, si un mod utilise le même nom qu'un autre mod, même si un tiers,
il doit avoir une API compatible.
Par exemple, si un jeu comprend un mod appelé «portes», il devrait avoir le
même API que `doors` dans Minetest Game.

La compatibilité API pour un mod est la somme des éléments suivants:

* Lua API table - Toutes les fonctions documentées / publiées dans la table globale qui partage le même nom.
		Par exemple, `mobs.register_mob`.
* Registered Nodes/Items -  La présence d'éléments.

Les petites ruptures ne sont pas si mauvaises, comme ne pas avoir d'utilitaire aléatoire
fonction qui n'était réellement utilisée qu'en interne, mais de plus grandes ruptures
liés aux fonctionnalités de base sont très mauvais.

Il est difficile de maintenir la compatibilité de l'API avec un méga-mod dégoûtant comme
*par défaut* dans Minetest Game, auquel cas le jeu ne doit pas inclure un mod nommé
défaut.

La compatibilité avec l'API s'applique également à d'autres mods et jeux tiers,
essayez donc de vous assurer que tous les nouveaux mods ont un nom de mod unique.
Pour vérifier si un nom de mod a été pris, recherchez-le sur
[content.minetest.net] (https://content.minetest.net/).

### Groupes and Alias

Les groupes et les alias sont tous deux des outils utiles pour maintenir la compatibilité entre les jeux,
car il permet aux noms d'objets d'être différents entre les différents jeux. Noeuds communs
comme la pierre et le bois doivent avoir des groupes pour indiquer le matériau. C'est aussi un
bonne idée de fournir des alias des nœuds par défaut à tous les remplacements directs.

## À ton tour !

* Créez un jeu simple où le joueur gagne des points en creusant des blocs spéciaux.

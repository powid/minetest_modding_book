---
title: Scripter en Lua
layout: default
root: ../..
idx: 1.2
description: Une intrduction aux bases du Lua, incluant les portées locales et globales.
redirect_from: /fr/chapters/lua.html
---

## Introduction  <!-- omit in toc -->

Dans ce chapitre nous allons parler de scripter en Lua, des outils prérequis et voir des techniques que vous trouverez probablement très utiles.

- [Editeurs de Code](#editeurs-de-code)
- [Coder en Lua](#coder-en-lua)
  - [Structure du Programme](#structure-du-programme)
  - [Types de Variables](#types-de-variables)
  - [Opérateurs Arithmétiques](#opérateurs-arithmétiques)
  - [Sélection](#sélection)
  - [Opérateurs Logiques](#opérateurs-logiques)
- [Programmer](#programmer)
- [Portée Locale et Portée Globale](#portée-locale-et-portée-globale)
  - [Les variables locales doivent être utilisées autant que possible](#les-variables-locales-doivent-être-utilisées-autant-que-possible)
- [Inclure d'autres scripts Lua](#inclure-d-autres-scripts-lua)

## Editeurs de Code

<<<<<<< HEAD
Un éditeur de code avec colorisation syntaxique est suffisant pour écrire des scripts en Lui.
=======
Un éditeur de code avec colorisation syntaxique est suffisant pour écrire des scripts en Lua.
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063
La colorisation syntaxique attribut différentes couleurs aux différents mods et charactères en fonction de leur signification. Cela vous permet d'identifier les erreurs.

```lua
function ctf.post(team,msg)
    if not ctf.team(team) then
        return false
    end
    if not ctf.team(team).log then
        ctf.team(team).log = {}
    end

    table.insert(ctf.team(team).log,1,msg)
    ctf.save()

    return true
end
```

Par exemple, les mots clés dans la bribe de code ci-dessus sont colorisé par exemple if, then, end et return.
table.insert est une fonction fournit par défaut par Lua.

Voici une liste d'éditeurs courants, adaptés au Lua.
Évidemment, d'autres éditeurs sont disponibles.

* Windows: [Notepad++](http://notepad-plus-plus.org/), [Atom](http://atom.io/), [VS Code](https://code.visualstudio.com/)
* Linux: Kate, Gedit, [Atom](http://atom.io/), [VS Code](https://code.visualstudio.com/)
* OSX: [Atom](http://atom.io/), [VS Code](https://code.visualstudio.com/)

## Coder en Lua

### Structure du Programme

Les programmes sont des suites de commande qui se lancent l'une après l'autre.
Nous appellons ces commandes des « instructions ».
La structure du programme est comment ces instructions vont être exécutées.
Différents types de structures vous permettent de sauter des séries de commandes.
Il y a trois types principaux de structures :

* Séquenciel : Lance une instruction après l'autre sans saut. 
* Sélection : Saute des séquence en fonction des conditions.
* Itératif : Répéter en boucle. Lance la même instruction jusqu'à qu'une condition soit remplie.

À quoi ressemble les instructions en Lua ?

```lua
local a = 2     -- Affecte 2 à 'a'
local b = 2     -- Affecte 2 à 'b'
local resultat = a + b -- Affecte a + b, qui vaut 4, à 'resultat'.
a = a + 10 -- Affecte a + 10, qui vaut 12, à la variable 'a'.
print("La somme vaut "..resultat)
```

<<<<<<< HEAD
Wahou, qu'est-ce qui s'est passé ici ?
=======
Wahou, mais que s'est-il passé ici ? Voici quelques explications :
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063

a, b et resultat sont des *variables*. Les variables locales sont déclarées en utilisant le mot clé `local` et en donnant une valeur initiale.
La portée « locale » sera un sujet prochain, c'est une partie très importante d'un concepte appelé « portée ».

Le `=` signifie « affecter », donc `resultat = a + b` signifie « affecter a + b à “resultat” ».
Contrèrement aux mathématiques et comme nous l'avons vu avec la variable `resultat`, les noms de variables peuvent être plus longs qu'un simple charactère.
Il est important de noter que Lua est sensible à la casse ; A est une variable différente de a.

### Types de Variables

Une variable sera forcément d'un (seul) type parmis les suivants. Elle peut cependant changer de type après une affectation.
Cependant, il est préférable de s'assurer qu'une variable garde toujours le même type ou le type Nil.

| Type     | Description                     | Exemple        |
|----------|---------------------------------|----------------|
| Nil      | Non initialisée. La variable est vide, elle n'a aucune valeur | `local A`, `D = nil` |
| Nombre   | Un nombre entier ou décimal.  | `local A = 4` |
| String   | Un morceau de texte  | `local D = "un deux trois"` |
| Boolean  | True (= Vrai) ou False (= Faux)    | `local est-ce_vrai = false`, `local E = (1 == 1)` |
| Table    | Listes | Expliqué plus bas |
| Function | Fonction ; Peut être lancée. Peut nécessiter des paramètres d'entrées et peut retourner une valeur | `local resultat = func(1, 2, 3)` |

### Opérateurs Arithmétiques

Liste non-exhaustive. Ne contient pas tous les opérateurs possibles.

| Symbole | But            | Exemple                   |
|---------|----------------|---------------------------|
| A + B   | Addition       | 2 + 2 = 4                 |
| A - B   | Soustraction   | 2 - 10 = -8               |
| A * B   | Multiplication | 2 * 2 = 4                 |
| A / B   | Division       | 100 / 50 = 2              |
| A ^ B   | Puissance      | 2 ^ 2 = 2<sup>2</sup> = 4 |
| A .. B  | Concaténer     | "toto" .. "ta" = "totota" |

### Sélection

La sélection la plus basique est l'instruction `if` (= si). Ça ressemble à ça :

```lua
local nombre_aleatoire = math.random(1, 100) -- tire un nombre aléatoire de 1 à 100.
if random_number > 50 then
    print("Wouhou !")
else
    print("Non !")
end
```

Cet exemple génère un nombre aléatoire de 1 à 100.
Ensuite, le programme affiche « Wouhou ! » si ce nombre est plus grand que 50. Sinon, il affiche « Non ! ».
Quels autres opérateurs y-a-t'il que `>` ?

### Opérateurs Logiques
*rappel : `true` = vrai ; `false` = faux.*

| Symbole  | But                                                | Exemple                                                     |
|----------|----------------------------------------------------|-------------------------------------------------------------|
| A == B   | Égal                                               | 1 == 1 (true), 1 == 2 (false)                               |
| A ~= B   | Innégal                                            | 1 ~= 1 (false), 1 ~= 2 (true)                               |
| A > B    | Plus grand que                                     | 5 > 2 (true), 1 > 2 (false), 1 > 1 (false)                  |
| A < B    | Plus petit que                                     | 1 < 3 (true), 3 < 1 (false), 1 < 1 (false)                  |
| A >= B   | Plus grand ou égal                                 | 5 >= 5 (true), 5 >= 3 (true), 5 >= 6 (false)                |
| A <= B   | Plus petit ou égal                                 | 3 <= 6 (true), 3 <= 3 (true)                                |
| A and B  | ET logique (les deux doivent être vrais)           | (2 > 1) and (1 == 1)  (true), (2 > 3) and (1 == 1)  (false) |
| A or B   | OU logique (l'un des deux au moins doit être vrai) | (2 > 1) or (1 == 2) (true), (2 > 4) or (1 == 3) (false)     |
| not A    | Pas vrai (ie: faux)                                | not (1 == 2)  (true), not (1 == 1)  (false)                 |

<<<<<<< HEAD
Le tableaune liste pas tous les opérateurs possible et il est possible de combiner les opérateurs comme suit :
=======
Le tableau ne liste pas tous les opérateurs possible et il est possible de combiner les opérateurs comme suit :
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063

```lua
if not A and B then
    print("Yeah !")
end
```

Qui affiche « Yeah ! » si A est faux (false) et B est vrai (true).

Les opérateurs arithmetiques et logiques fonctionnent exactement de la même manière ;
Les deux acceptent des entrées et peuvent retourner des valeurs qui peuvent être stockées.

```lua
local A = 5
local is_equal = (A == 5) -- "Is equal" signifie « Est égal » en français.
if is_equal then
    print("C'est égal !")
end
```

## Programmer

Programmer est l'action de prendre un problème, comme trier une liste d'objets, et ensuite le transformer en une suite d'étapes que l'ordinateur comprendra.

Vous enseigner le processus logique de la programmation outrepasse la portée de ce livre ;
Cependant, les sites suivant seront très utiles pour le développer :

* [Openclassroom](https://openclassrooms.com/fr/) est l'une des meilleures ressources pour apprendre le « code », il propose une expérience d'apprentissage avec des travaux pratiques.
* [Scratch](https://scratch.mit.edu) est une bonne ressource pour débuter la programmation depuis la base, apprendre les techniques de résolutions de problèmes, requises pour programmer.\\
  Scratch est **conçu pour apprendre aux enfants** comment programmer et n'est pas un langage de programmation sérieux.

## Portée Locale et Portée Globale

<<<<<<< HEAD
Qu'une variable soit locale ou globale détermine où il sera possible de la lire ou d'y écrire.
=======
Quand une variable est locale ou globale, cela détermine où il sera possible de la lire ou d'y écrire.
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063
Une variable locale n'est accessible que depuis là où elle est définie. Voici quelques exemples :

```lua
-- Accessible depuis l'intérieur de ce fichier script
local un = 1

function myfunc()
    -- Accessible depuis l'intérieur de cette fonction
    local deux = un + un

    if two == one then
        -- Accessible depuis l'intérieur de cette instruction if
        local trois = un + deux
    end
end
```

<<<<<<< HEAD
Alors que les variables globales sont accessible depuis n'importe où dans le fichier script et depuis n'importe quel autre mod.
=======
Alors que les variables globales sont accessibles depuis n'importe où dans le fichier script et depuis n'importe quel autre mod.
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063

```lua
ma_variable_globale = "blah"

function un()
    ma_variable_globale = "trois"
end

print(ma_variable_globale) -- Sortie : "blah"
un()
print(ma_variable_globale) -- Sortie : "trois"
```


### Les variables locales doivent être utilisées autant que possible

Le Lua fonctionne par défaut en global (contrairement à la majorité des autres langages de programmation).
Les variables locales doivent être identifiées comme tel.

```lua
function un()
    toto = "tata"
end

function deux()
    print(dump(toto))  -- Sortie : "tata"
end

un()
deux()
```

dump() est une fonction qui peut transformer n'importe quelle variable en type `string` pour que le programmeur puisse voir son contenu.
La variable toto sera affichée comme « "tata" », incluant les guillemets `"` qui montrent qu'il s'agit d'une variable de type `string`.

C'est un code bâclé et dans les faits, Minetest vous avertira à ce propos :

    Assignment to undeclared global 'toto' inside function at init.lua:2

Pour corriger cette erreur, utilisez « local » :

```lua
function un()
   local toto = "tata"
end

function deux()
    print(dump(toto))  -- Sortie : nil
end

un()
deux()
```

<<<<<<< HEAD
Souvenez-vous que nil veut dire **non initialisée**.
=======
Souvenez-vous que « nil » veut dire **non initialisée**.
>>>>>>> 0e9e625a039dc4f0eaac6d61400f164f48d5e063
La variable n'a pas encore été affectée d'une valeur, elle n'existe pas ou a été dé-inilialisée (ie : affectée de nil).

Il en est de même pour les fonctions. Les fonctions sont des variables d'un type particulier et doivent être faites locales pour que les autres mods puissent avoir des fonctions du même nom.

```lua
local function toto(tata)
    return tata * 2
end
```

Les tables API doivent être utilisées pour permettrent à d'autres mods d'appeler les fonctions, comme suit :

```lua
monmod = {}

function monmod.toto(tata)
    return "toto" .. tata
end

-- Dans un autre mod ou script :
monmod.toto("tititata")
```

## Inclure d'autres scripts Lua

La manière recommendée pour inclure d'autres scripts Lua dans un mod est d'utiliser *dofile*.

```lua
dofile(minetest.get_modpath("modname") .. "/script.lua")
```

Les variabes « locales » déclarés en dehors de toute fonction dans un fichier script seront locales à ce script.
Un script peut retourner une valeur, ce qui est peut être très utile pour partager des locales privées.

```lua
-- script.lua
return "Salut le monde !"

-- init.lua
local ret = dofile(minetest.get_modpath("modname") .. "/script.lua")
print(ret) -- Salut le monde !
```

Les prochains chapitres traiteront de comment séparer le code d'un mod plus en détail.
Cependant, l'approche simpliste actuelle est d'avoir différents fichiers pour différents types de choses. - nodes.lua, crafts.lua, craftitems.lua, etc.

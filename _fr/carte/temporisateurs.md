---
title: Temporisateurs de noeud et ABMs
layout: default
root: ../..
idx: 3.2
description: Apprenez à créer des ABMs pour modifier des blocs.
redirect_from:
- /en/chapters/abms.html
- /en/map/abms.html
---

## Introduction <!-- omit in toc -->

L'exécution périodique d'une fonction sur certains nœuds est une tâche courante.
Minetest propose deux méthodes: les modificateurs de blocs actifs (ABM) et les temporisateurs de nœuds.

Les GAB analysent tous les MapBlocks chargés à la recherche de nœuds correspondant à un critère.
Ils conviennent mieux aux nœuds que l'on trouve fréquemment dans le monde,
comme l'herbe.
Ils ont une surcharge de CPU élevée, mais une faible mémoire et surcharge de stockage.

Pour les nœuds rares ou qui utilisent déjà des métadonnées, comme les fours
et les machines, les temporisateurs de nœuds doivent être utilisés à la place.
Les temporisateurs de noeud fonctionnent en gardant une trace des temporisateurs en attente dans chaque MapBlock, puis
les faire courir à leur expiration.
Cela signifie que les temporisateurs n'ont pas besoin de rechercher tous les nœuds chargés pour trouver des correspondances,
mais nécessitent plutôt un peu plus de mémoire et de stockage pour le suivi
des minuteries en attente.

- [Heures du nœuds](#node-timers)
- [Modificateurs de blocs actifs](#active-block-modifiers)
- [À ton tour](#your-turn)

## Heures du nœuds

Les temporisateurs de noeud sont directement liés à un seul noeud.
Vous pouvez gérer les temporisateurs de noeud en obtenant un objet NodeTimerRef.

```lua
local timer = minetest.get_node_timer(pos)
timer:start(10.5) -- in seconds
```

Vous pouvez également vérifier l'état ou arrêter le chronomètre:

```lua
if timer:is_started() then
    print("The timer is running, and has " .. timer:get_timeout() .. "s remaining!")
    print(timer:get_elapsed() .. "s has elapsed.")
end

timer:stop()
```

Lorsqu'un temporisateur de noeud est en marche, la méthode `on_timer` dans la table de définition du noeud
être appelé.
La méthode ne prend qu'un seul paramètre, la position du nœud.

```lua
minetest.register_node("autodoors:door_open", {
    on_timer = function(pos)
        minetest.set_node(pos, { name = "autodoors:door" })
        return false
    end
})
```

Renvoyer true dans `on_timer` provoquera une nouvelle exécution du temporisateur pour le même intervalle.

Vous avez peut-être remarqué une limitation avec les minuteries: pour des raisons d'optimisation, il est
seulement possible d'avoir un type de temporisateur par type de nœud, et un seul temporisateur en cours d'exécution par nœud.


## Modificateurs de blocs actifs

Aux fins du présent chapitre, l'herbe étrangère est un type d'herbe qui
a une chance d'apparaître près de l'eau.


```lua
minetest.register_node("aliens:grass", {
    description = "Alien Grass",
    light_source = 3, -- The node radiates light. Min 0, max 14
    tiles = {"aliens_grass.png"},
    groups = {choppy=1},
    on_use = minetest.item_eat(20)
})

minetest.register_abm({
    nodenames = {"default:dirt_with_grass"},
    neighbors = {"default:water_source", "default:water_flowing"},
    interval = 10.0, -- Run every 10 seconds
    chance = 50, -- Select every 1 in 50 nodes
    action = function(pos, node, active_object_count,
            active_object_count_wider)
        local pos = {x = pos.x, y = pos.y + 1, z = pos.z}
        minetest.set_node(pos, {name = "aliens:grass"})
    end
})
```

Cet ABM s'exécute toutes les dix secondes, et pour chaque nœud correspondant, il y a
1 chance sur 50 de fonctionner.
Si l'ABM s'exécute sur un nœud, un nœud herbe étrangère est placé au-dessus.
Veuillez être averti, cela supprimera tout nœud précédemment situé dans cette position.
Pour éviter cela, vous devez inclure une vérification en utilisant minetest.get_node pour vous assurer qu'il y a de la place pour l'herbe.

La spécification d'un voisin est facultative.
Si vous spécifiez plusieurs voisins, un seul d'entre eux doit être
présent pour répondre aux exigences.

La spécification du hasard est également facultative.
Si vous ne spécifiez pas la chance, l'ABM s'exécutera toujours lorsque les autres conditions seront remplies.

## À ton tour

* Touche Midas: faites en sorte que l'eau se transforme en blocs d'or avec 1 chance sur 100, toutes les 5 secondes.
* Pourriture: Faites du bois se transformer en saleté lorsque l'eau est un voisin.
* Burnin ': faites en sorte que chaque nœud aérien prenne feu. (Astuce: "air" et "fire: basic_flame").
   Attention: attendez-vous à ce que le jeu plante.

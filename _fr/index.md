---
title: Couverture
layout: default
homepage: true
no_header: true
root: ..
idx: 0.1
---

<header>
    <h1>Livre de Modding Minetest</h1>

    <span>Originalement par <a href="https://rubenwardy.com" rel="author">rubenwardy</a></span>
    <span>avec l'édition de <a href="http://rc.minetest.tv/">Shara</a></span>
    <span>et la traduction de <a href="https://powi.fr/">Powi</a> et <a href="https://www.svtux.fr">Sangokuss</a></span>
</header>

## Introduction

Minetest utilise des scripts Lua pour le modding.
Ce livre a pour but de vous apprendre à créer vos propres mods en commençant par les bases.
Chaque chapitre sera orienté autour d'une partie de l'API en particulier et vous apprendrez rapidement à réaliser vos propres mods.

En plus de [lire ce livre en ligne](https://rubenwardy.com/minetest_modding_book/fr),
vous pouvez aussi le [télécharger dans sa version html](https://github.com/rubenwardy/minetest_modding_book/releases).

### Retours et Contributions

Vous avez remarqué une erreur et voulez la remonter ? Merci de m'informer.

* Créer une [remontée GitLab](https://gitlab.com/rubenwardy/minetest_modding_book/issues).
* Poster sur le [sujet du Forum](https://forum.minetest.net/viewtopic.php?f=14&t=10729).
* [Contactez-moi](https://rubenwardy.com/contact/).
* Vous souhaitez contribuer ?
  [Lisez le README](https://github.com/rubenwardy/minetest_modding_book/blob/master/README.md).

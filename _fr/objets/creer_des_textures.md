---
title: Création de Textures
layout: default
root: ../..
idx: 2.2
description: Une introduction à la création de textures dans l'éditeur de votre choix, un guide sur GIMP.
redirect_from: /en/chapters/creating_textures.html
---

## Introduction <!-- omit in toc -->

Être capable de créer et d'optimiser des textures est une compétence très utile quand on fait du développement pour Minetest.
Il existe de nombreuses techniques pertinentes pour travailler sur les textures pixel-art,
et la compréhension de ces techniques améliorera considérablement
la qualité des textures que vous créez.

Les approches détaillées pour créer un bon pixel-art sont hors de portée
de ce livre, et seulement les techniques de base les plus pertinentes
seront couvert.

Il y a de nombreux [excellents tutoriels en ligne](http://www.photonstorm.com/art/tutorials-art/16x16-pixel-art-tutorial) disponibles, qui couvrent le pixel art de manière beaucoup plus détaillée.

- [Techniques](#techniques)
  - [Utilisation du crayon](#using-the-pencil)
  - [Carrelage](#tiling)
  - [Transparence](#transparency)
- [Editeurs](#editors)
  - [MS Paint](#ms-paint)
  - [GIMP](#gimp)

## Techniques

### Utilisation du crayon

L'outil crayon est disponible dans la plupart des éditeurs. Lorsqu'il est réglé sur sa taille la plus basse, il vous permet d'éditer un pixel à la fois sans changer aucune autre partie de l'image. En manipulant les pixels un par un, vous créez des
et des textures nettes sans flou involontaire. Cela vous donne également un niveau de précision et de contrôle.

### Carrelage

Les textures utilisées pour les nœuds doivent généralement être conçues pour être carrelées. Ça signifie que 
lorsque vous placez plusieurs nœuds avec la même texture ensemble, l'alignement des bords doit être correct.

<!-- IMAGE NEEDED - cobblestone that tiles correctly -->

Si vous ne parvenez pas à faire correspondre les bords correctement, le résultat est beaucoup moins agréable à regarder.

<!-- IMAGE NEEDED - node that doesn't tile correctly -->

### Transparence

La transparence est importante lors de la création de textures pour presque tous les objets d'artisanat et certains nœuds, comme le verre.
Tous les éditeurs ne prennent pas en charge la transparence, alors assurez-vous de choisir un éditeur qui convient aux textures que vous souhaitez créer.

## Editeurs

### MS Paint

MS Paint est un éditeur simple qui peut être utile pour la texture de base conception; cependant, il ne prend pas en charge la transparence.
Cela n'a généralement pas d'importance lors de la création de textures pour les côtés des nœuds, mais si vous avez besoin de transparence dans vos textures, vous devez choisir un
éditeur différent.

### GIMP

GIMP est couramment utilisé dans la communauté Minetest. Son apprentissage demande un peu de temps et de volonté car nombre de ses fonctionnalités ne sont pas immédiatement
évident, mais les résultats sont à la hauteur de ceux-ci.

Lorsque vous utilisez GIMP, l'outil crayon peut être sélectionné dans la boîte à outils:

<figure>
    <img src="{{ page.root }}//static/pixel_art_gimp_pencil.png" alt="Crayon dans GIMP">
</figure>

Il est également conseillé de cocher la case Bord dur pour l'outil Gomme.
---
title: Objets, Joueurs, et Entités
layout: default
root: ../..
idx: 3.4
description: Utiliser un ObjectRef
degrad:
    level: warning
    title: Degrés et radians
    message: La rotation des pièces jointes est définie en degrés, tandis que la rotation des objets est en radians.
             Assurez-vous de convertir à la mesure d'angle correcte.
---

## Introduction <!-- omit in toc -->

Dans ce chapitre, vous apprendrez à manipuler des objets et à définir votre
posséder.

- [Que sont les objets, les joueurs et les entités?](#what-are-objects-players-and-entities)
- [Position et vitesse](#position-and-velocity)
- [Propriétés des objets](#object-properties)
- [Entités](#entities)
- [Pièces jointes](#attachments)
- [À ton tour](#your-turn)

## Que sont les objets, les joueurs et les entités?

Les joueurs et les entités sont les deux types d'objets. Un objet est quelque chose qui peut bouger
indépendamment de la grille de nœuds et possède des propriétés telles que la vitesse et l'échelle.
Les objets ne sont pas des objets et ils ont leur propre système d'enregistrement distinct.

Il existe quelques différences entre les joueurs et les entités.
Le plus important est que les joueurs sont contrôlés par les joueurs, tandis que les entités sont contrôlées par les mods.
Cela signifie que la vitesse d'un joueur ne peut pas être définie par les mods - les joueurs sont côté client,
et les entités sont côté serveur.
Une autre différence est que les joueurs entraîneront le chargement des blocs de carte, tandis que les entités
sera simplement enregistré et deviendra inactif.

Cette distinction est brouillée par le fait que les entités sont contrôlées à l'aide d'un tableau
qui est appelé une luaentity, comme discuté plus loin.

## Position et vitesse

`get_pos` et` set_pos` existent pour vous permettre d'obtenir et de définir la position d'une entité.

```lua
local object = minetest.get_player_by_name("bob")
local pos    = object:get_pos()
object:set_pos({ x = pos.x, y = pos.y + 1, z = pos.z })
```

`set_pos` définit immédiatement la position, sans animation. Si vous souhaitez
animer en douceur un objet à la nouvelle position, vous devez utiliser `move_to`.
Cela, malheureusement, ne fonctionne que pour les entités.

```lua
object:move_to({ x = pos.x, y = pos.y + 1, z = pos.z })
```

Une chose importante à considérer lors du traitement des entités est la latence du réseau.
Dans un monde idéal, les messages sur les mouvements d'entité arriveraient immédiatement,
dans le bon ordre et avec un intervalle similaire quant à la façon dont vous les avez envoyés.
Cependant, à moins que vous ne soyez en solo, ce n'est pas un monde idéal.
Les messages mettront un certain temps à arriver. Les messages de position peuvent arriver dans le désordre,
entraînant le saut de certains appels `set_pos` car il est inutile de
une position plus ancienne que la position connue actuelle.
Les mouvements peuvent ne pas être espacés de manière similaire, ce qui rend difficile leur utilisation pour l'animation.
Tout cela fait que le client voit différentes choses sur le serveur, ce qui est quelque chose
vous devez être au courant.

## Propriétés des objets

Contrairement aux nœuds, les objets ont une apparence dynamique plutôt que définie.
Vous pouvez modifier l'apparence d'un objet, entre autres, à tout moment en mettant à jour
ses propriétés.

```lua
object:set_properties({
    visual      = "mesh",
    mesh        = "character.b3d",
    textures    = {"character_texture.png"},
    visual_size = {x=1, y=1},
})
```

Les propriétés mises à jour seront envoyées à tous les joueurs à portée.
Ceci est très utile pour obtenir une grande quantité de variétés à très bas prix, comme
skins différents par joueur.

Comme indiqué dans la section suivante, les entités peuvent avoir des propriétés initiales
fournis dans leur définition.
Les propriétés du lecteur par défaut sont définies dans le moteur, donc vous
besoin d'utiliser `set_properties ()` dans `on_joinplayer` pour définir les propriétés
rejoint les joueurs.

## Entités

Une entité a une table de types un peu comme un élément.
Ce tableau peut contenir des méthodes de rappel, des propriétés d'objet par défaut et des éléments personnalisés.

```lua
local MyEntity = {
    initial_properties = {
        hp_max = 1,
        physical = true,
        collide_with_objects = false,
        collisionbox = {-0.3, -0.3, -0.3, 0.3, 0.3, 0.3},
        visual = "wielditem",
        visual_size = {x = 0.4, y = 0.4},
        textures = {""},
        spritediv = {x = 1, y = 1},
        initial_sprite_basepos = {x = 0, y = 0},
    },

    message = "Default message",
}

function MyEntity:set_message(msg)
    self.message = msg
end
```

Lorsqu'une entité a émergé, une table est créée pour elle en copiant tout depuis
sa table de types.
Ce tableau peut être utilisé pour stocker des variables pour cette entité particulière.

Un ObjectRef et une table d'entités fournissent des moyens d'obtenir la contrepartie:


```lua
local entity = object:get_luaentity()
local object = entity.object
print("entity is at " .. minetest.pos_to_string(object:get_pos()))
```

Il existe un certain nombre de rappels disponibles à utiliser avec les entités.
Une liste complète peut être trouvée dans [lua_api.txt]({{page.root}}/lua_api.html # registered-entity).

```lua
function MyEntity:on_step(dtime)
    local pos      = self.object:get_pos()
    local pos_down = vector.subtract(pos, vector.new(0, 1, 0))

    local delta
    if minetest.get_node(pos_down).name == "air" then
        delta = vector.new(0, -1, 0)
    elseif minetest.get_node(pos).name == "air" then
        delta = vector.new(0, 0, 1)
    else
        delta = vector.new(0, 1, 0)
    end

    delta = vector.multiply(delta, dtime)

    self.object:move_to(vector.add(pos, delta))
end

function MyEntity:on_punch(hitter)
    minetest.chat_send_player(hitter:get_player_name(), self.message)
end
```

Maintenant, si vous deviez engendrer et utiliser cette entité, vous remarqueriez que le message
serait oublié lorsque l'entité redevient inactive puis redevient active.
En effet, le message n'est pas enregistré.
Plutôt que de tout enregistrer dans la table d'entités, Minetest vous donne le contrôle sur
comment sauver des choses.
Staticdata est une chaîne qui contient toutes les informations personnalisées
doit être stocké.

```lua
function MyEntity:get_staticdata()
    return minetest.write_json({
        message = self.message,
    })
end

function MyEntity:on_activate(staticdata, dtime_s)
    if staticdata ~= "" and staticdata ~= nil then
        local data = minetest.parse_json(staticdata) or {}
        self:set_message(data.message)
    end
end
```

Minetest peut appeler `get_staticdata ()` autant de fois qu'il le souhaite et à tout moment.
En effet, Minetest n'attend pas qu'un MapBlock devienne inactif pour enregistrer
, car cela entraînerait une perte de données. MapBlocks sont enregistrés à peu près tous les 18
secondes, vous devriez donc remarquer un intervalle similaire pour l'appel de `get_staticdata ()`.

`on_activate ()`, en revanche, ne sera appelé que lorsqu'une entité devient
actif soit à partir de l'activation de MapBlock, soit à partir de la création de l'entité.
Cela signifie que les données statiques peuvent être vides.

Enfin, vous devez enregistrer la table de types en utilisant le bien nommé `register_entity`.

```lua
minetest.register_entity("mymod:entity", MyEntity)
```

L'entité peut être engendrée par un mod comme ceci:

```lua
local pos = { x = 1, y = 2, z = 3 }
local obj = minetest.add_entity(pos, "mymod:entity", nil)
```

Le troisième paramètre est les données statiques initiales.
Pour définir le message, vous pouvez utiliser la méthode de table d'entités:

```lua
obj:get_luaentity():set_message("hello!")
```

Les joueurs avec le *donne* [privilège](../joueurs/privileges.html) peuvent
utilisez une [commande chat](../players/chat.html) pour générer des entités:

    /spawnentity mymod:entity

## Pièces jointes

Les objets attachés se déplaceront lorsque le parent - l'objet auquel ils sont attachés -
est déplacé. Un objet attaché serait un enfant du parent.
Un objet peut avoir un nombre illimité d'enfants, mais au plus un parent.

```lua
child:set_attach(parent, bone, position, rotation)
```

Un `get_pos ()` d'un objet retournera toujours la position globale de l'objet, non
importe qu'il soit joint ou non.
`set_attach` prend une position relative, mais pas comme vous vous y attendez.
La position de l'attachement est relative à l'origine du parent, multipliée par 10.
Donc, 0,5,0 serait un demi-nœud au-dessus de l'origine du parent.

{% include notice.html notice = page.degrad%}

Pour les modèles 3D avec animations, l'argument bone est utilisé pour attacher l'entité
à un os.
Les animations 3D sont basées sur des squelettes - un réseau d'os dans le modèle où
chaque os peut recevoir une position et une rotation pour changer le modèle, par exemple,
pour déplacer le bras.
L'attachement à un os est utile si vous voulez qu'un personnage retienne quelque chose:

```lua
obj:set_attach(player,
    "Arm_Right",           -- default bone
    {x=0.2, y=6.5, z=3},   -- default position
    {x=-100, y=225, z=90}) -- default rotation
```

## À ton tour

* Faites un moulin à vent en combinant des nœuds et une entité.
* Faites une foule de votre choix (en utilisant uniquement l'API d'entité, et sans utiliser d'autres mods).
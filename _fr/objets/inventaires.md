---
title: ItemStacks et inventaires
layout: default
root: ../..
idx: 2.4
description: Manipuler InvRefs et ItemStacks
redirect_from:
- /en/chapters/inventories.html
- /en/chapters/itemstacks.html
- /en/inventories/inventories.html
- /en/inventories/itemstacks.html
---

## Introduction  <!-- omit in toc -->

Dans ce chapitre, vous apprendrez à utiliser et à manipuler les inventaires, que ce soit
qui soit un inventaire de joueur, un inventaire de noeud ou un inventaire détaché.

- [Que sont les piles d'articles et les inventaires ?](#what-are-itemstacks-and-inventories)
- [ItemStacks](#itemstacks)
- [Emplacements d'inventaire](#inventory-locations)
- [Listes](#lists)
  - [Taille et largeur](#size-and-width)
  - [Vérification du contenu](#checking-contents)
- [Modification des inventaires et des piles d'articles](#modifying-inventories-and-itemstacks)
  - [Ajouter à une liste](#adding-to-a-list)
  - [Prendre des objets](#taking-items)
  - [Manipulation des piles](#manipulating-stacks)
- [Usure](#wear)
- [Lua Tables](#lua-tables)

## Que sont les piles d'articles et les inventaires ?

Un ItemStack est les données derrière une seule cellule dans un inventaire.

Un *inventaire* est une collection de *listes d'inventaire*, dont chacune
est une grille 2D d'ItemStacks.
Les listes d'inventaire sont simplement appelées *listes* dans le contexte
des inventaires.
Le but d'un inventaire est de permettre plusieurs grilles lorsque les joueurs
et les nœuds ne contiennent qu’un seul inventaire.

## ItemStacks

Les ItemStacks ont trois composants.

Le nom de l'élément peut être le nom d'un élément enregistré, un alias ou un inconnu
nom de l'article.
Les éléments inconnus sont courants lorsque les utilisateurs désinstallent des mods ou lorsque les mods suppriment des éléments sans
précautions, telles que l'enregistrement d'alias.

```lua
print(stack:get_name())
stack:set_name("default:dirt")

if not stack:is_known() then
    print("Is an unknown item!")
end
```

Le nombre sera toujours égal ou supérieur à 0.
Grâce à un gameplay normal, le nombre ne doit pas dépasser la taille maximale de la pile
de l'élément - `stack_max`.
Cependant, les commandes d'administration et les mods buggy peuvent entraîner des piles dépassant le maximum
Taille.

```lua
print(stack:get_stack_max())
```




Un ItemStack peut être vide, auquel cas le compte sera 0.

```lua
print(stack:get_count())
stack:set_count(10)
```

ItemStacks peut être construit de plusieurs façons à l'aide de la fonction ItemStack.

```lua
ItemStack() -- name="", count=0
ItemStack("default:pick_stone") -- count=1
ItemStack("default:stone 30")
ItemStack({ name = "default:wood", count = 10 })
```

Les métadonnées d'élément sont un magasin de valeurs-clés illimité pour les données sur l'élément.
La valeur-clé signifie que vous utilisez un nom (appelé la clé) pour accéder aux données (appelé la valeur).
Certaines clés ont une signification spéciale, comme `description` qui est utilisée pour avoir une pile par pile
description de l'article.
Cela sera traité plus en détail dans le chapitre Métadonnées et stockage.

## Emplacements d'inventaire

Un emplacement d'inventaire est où et comment l'inventaire est stocké.
Il existe trois types d'emplacement d'inventaire: joueur, nœud et détaché.
Un inventaire est directement lié à un et un seul emplacement - mise à jour de l'inventaire
entraînera sa mise à jour immédiate.

Les inventaires de nœuds sont liés à la position d'un nœud spécifique, tel qu'un coffre.
Le nœud doit être chargé car il est stocké dans [node metadata](../map/storage.html#metadata).

```lua
local inv = minetest.get_inventory({ type="node", pos={x=1, y=2, z=3} })
```

Ce qui précède obtient une *référence d'inventaire*, communément appelée *InvRef*.
Les références d'inventaire sont utilisées pour manipuler un inventaire.
*Référence* signifie que les données ne sont pas réellement stockées à l'intérieur de cet objet,
mais à la place, l'objet met directement à jour les données sur place.

L'emplacement d'une référence d'inventaire peut être trouvé comme suit:

```lua
local location = inv:get_location()
```

Les inventaires des joueurs peuvent être obtenus de la même manière ou en utilisant une référence de joueur.
Le joueur doit être en ligne pour accéder à son inventaire.


```lua
local inv = minetest.get_inventory({ type="player", name="player1" })
-- or
local inv = player:get_inventory()
```

Un inventaire détaché est un inventaire indépendant des joueurs ou des nœuds.
Les inventaires détachés ne sauvegardent pas non plus lors d'un redémarrage.
Des inventaires détachés doivent être créés avant de pouvoir être utilisés -
cela sera traité plus tard.

```lua
local inv = minetest.get_inventory({
    type="detached", name="inventory_name" })
```

Contrairement aux autres types d'inventaire, vous devez d'abord créer un inventaire détaché:

```lua
minetest.create_detached_inventory("inventory_name")
```

La fonction create_detached_inventory accepte 3 arguments, seul le premier est requis.
Le deuxième argument prend une table de rappels, qui peut être utilisée pour contrôler la façon dont
les joueurs interagissent avec l'inventaire:

```lua
-- Input only detached inventory
minetest.create_detached_inventory("inventory_name", {
    allow_move = function(inv, from_list, from_index, to_list, to_index, count, player)
        return count -- allow moving
    end,

    allow_put = function(inv, listname, index, stack, player)
        return stack:get_count() -- allow putting
    end,

    allow_take = function(inv, listname, index, stack, player)
        return -1 -- don't allow taking
    end,

    on_put = function(inv, listname, index, stack, player)
        minetest.chat_send_all(player:get_player_name() ..
            " gave " .. stack:to_string() ..
            " to the donation chest at " .. minetest.pos_to_str(pos))
    end,
})
```

Rappels d'autorisation - c'est-à-dire: ceux commençant par `allow_`- renvoient le numéro
d'éléments à transférer, -1 étant utilisé pour empêcher complètement le transfert.

Les rappels d'action - commençant par `on_` - n'ont pas de valeur de retour et
ne peut pas empêcher les transferts.

## Listes

Les listes d'inventaire sont un concept utilisé pour permettre à plusieurs grilles d'être stockées dans un même emplacement.
Ceci est particulièrement utile pour le joueur car il existe un certain nombre de listes communes
que tous les jeux ont, comme l'inventaire *principal* et les emplacements *d'artisanat*.


### Taille et largeur

Les listes ont une taille, qui est le nombre total de cellules de la grille, et une largeur,
qui n'est utilisé que dans le moteur.
La largeur de la liste n'est pas utilisée lors du dessin de l'inventaire dans une fenêtre,
car le code derrière la fenêtre détermine la largeur à utiliser.

```lua
if inv:set_size("main", 32) then
    inv:set_width("main", 8)
    print("size:  " .. inv.get_size("main"))
    print("width: " .. inv:get_width("main"))
else
    print("Error! Invalid itemname or size to set_size()")
end
```

`set_size` échouera et retournera false si le nom de liste ou la taille n'est pas valide.
Par exemple, la nouvelle taille peut être trop petite pour s'adapter à tous les éléments actuels
dans l'inventaire.

### Vérification du contenu

`is_empty` peut être utilisé pour voir si une liste contient des éléments:
```lua
if inv:is_empty("main") then
    print("The list is empty!")
end
```

`contains_item` peut être utilisé pour voir si une liste contient un élément spécifique.

## Modification des inventaires et des piles d'articles

### Ajouter à une liste

Pour ajouter des éléments à une liste nommée "" principale "` tout en respectant les tailles de pile maximales:

```lua
local stack    = ItemStack("default:stone 99")
local leftover = inv:add_item("main", stack)
if leftover:get_count() > 0 then
    print("Inventory is full! " ..
            leftover:get_count() .. " items weren't added")
end
```

### Prendre des objets

Pour supprimer des éléments d'une liste:
```lua
local taken = inv:remove_item("main", stack)
print("Took " .. taken:get_count())
```

### Manipulation des piles

Vous pouvez modifier des piles individuelles en les obtenant d'abord:
```lua
local stack = inv:get_stack(listname, 0)
```

Puis en les modifiant en définissant des propriétés ou en utilisant les méthodes qui
respecter `stack_size`:


```lua
local stack    = ItemStack("default:stone 50")
local to_add   = ItemStack("default:stone 100")
local leftover = stack:add_item(to_add)
local taken    = stack:take_item(19)

print("Could not add"  .. leftover:get_count() .. " of the items.")
-- ^ will be 51

print("Have " .. stack:get_count() .. " items")
-- ^ will be 80
--   min(50+100, stack_max) - 19 = 80
--     where stack_max = 99
```

`add_item` ajoutera des éléments à un ItemStack et retournera ceux qui n'ont pas pu être ajoutés.
`take_item` prendra le nombre d'articles mais peut en prendre moins et retourne la pile prise.

Enfin, définissez la pile d'objets:

```lua
inv:set_stack(listname, 0, stack)
```

## Usure

Les outils peuvent avoir de l'usure; l'usure montre une barre de progression et fait casser l'outil lorsqu'il est complètement usé.
L'usure est un nombre sur 65535; plus il est haut, plus l'outil est usé.

L'usure peut être manipulée en utilisant `add_wear ()`, `get_wear ()` et `set_wear (wear)`.

```lua
local stack = ItemStack("default:pick_mese")
local max_uses = 10

-- This is done automatically when you use a tool that digs things
-- It increases the wear of an item by one use.
stack:add_wear(65535 / (max_uses - 1))
```

Lors du creusement d'un nœud, le degré d'usure d'un outil peut dépendre du nœud
être creusé. Donc max_uses varie en fonction de ce qui est creusé.

## Lua Tables

ItemStacks et Inventories peuvent être convertis vers et depuis des tableaux.
Ceci est utile pour la copie et les opérations en bloc.

```lua
-- Entire inventory
local data = inv1:get_lists()
inv2:set_lists(data)

-- One list
local listdata = inv1:get_list("main")
inv2:set_list("main", listdata)
```

Le tableau des listes retourné par `get_lists ()` sera sous cette forme:

```lua
{
    list_one = {
        ItemStack,
        ItemStack,
        ItemStack,
        ItemStack,
        -- inv:get_size("list_one") elements
    },
    list_two = {
        ItemStack,
        ItemStack,
        ItemStack,
        ItemStack,
        -- inv:get_size("list_two") elements
    }
}
```

`get_list ()` retournera une seule liste comme juste une liste de ItemStacks.

Une chose importante à noter est que les méthodes définies ci-dessus ne modifient pas la taille
des listes.
Cela signifie que vous pouvez effacer une liste en la définissant sur une table vide et elle ne
diminution de la taille:

```lua
inv:set_list("main", {})
```

---
title: Générateur de commandes de chat
layout: default
root: ../..
idx: 4.3
description: Utilisez ChatCmdBuilder pour créer une commande de discussion complexe
redirect_from: /en/chapters/chat_complex.html
---

## Introduction <!-- omit in toc -->

Ce chapitre vous montrera comment créer des commandes de chat complexes avec ChatCmdBuilder,
tels que `/ msg <nom> <message>`, `/ team join <teamname>` ou `/ team Leave <teamname>`.

Notez que ChatCmdBuilder est une bibliothèque créée par l'auteur de ce livre, et la plupart
les moddeurs ont tendance à utiliser la méthode décrite dans le
[Chat and Commands](chat.html#complex-subcommands) chapitre.

- [Pourquoi ChatCmdBuilder?](#why-chatcmdbuilder)
- [Routes](#routes)
- [Fonctions de sous-commande](#subcommand-functions)
- [Installation de ChatCmdBuilder](#installing-chatcmdbuilder)
- [Commande complexe admin](#admin-complex-command)

## Pourquoi ChatCmdBuilder?

Traditionnellement, les mods implémentaient ces commandes complexes en utilisant des modèles Lua.

```lua
local name = string.match(param, "^join ([%a%d_-]+)")
```

Cependant, je trouve les motifs Lua ennuyeux à écrire et illisibles.
Pour cette raison, j'ai créé une bibliothèque pour le faire pour vous.

```lua
ChatCmdBuilder.new("sethp", function(cmd)
    cmd:sub(":target :hp:int", function(name, target, hp)
        local player = minetest.get_player_by_name(target)
        if player then
            player:set_hp(hp)
            return true, "Killed " .. target
        else
            return false, "Unable to find " .. target
        end
    end)
end, {
    description = "Set hp of player",
    privs = {
        kick = true
        -- ^ probably better to register a custom priv
    }
})
```

`ChatCmdBuilder.new (nom, setup_func, def)` crée une nouvelle commande de chat appelée
`nom`. Il appelle ensuite la fonction qui lui est passée (`setup_func`), qui crée ensuite
sous-commandes. Chaque `cmd: sub (route, func)` est une sous-commande.

Une sous-commande est une réponse particulière à un paramètre d'entrée. Quand un joueur court
la commande chat, la première sous-commande correspondant à leur entrée sera exécutée,
et pas d'autres. Si aucune sous-commande ne correspond, alors l'utilisateur sera informé de l'invalide
syntaxe. Par exemple, dans l'extrait de code ci-dessus si un joueur
tape quelque chose de la forme `/sethp username 12` puis la fonction est passée
à cmd: sub sera appelé. S'ils tapent `/sethp 12 bleh`, alors une erreur
un message d'entrée apparaîtra.

`: nom: hp: int` est une route. Il décrit le format du paramètre passé à /teleport.

## Routes

Un itinéraire est composé de terminaux et de variables. Les terminaux doivent toujours être là.
Par exemple, `joindre` dans` / rejoindre l'équipe: nom d'utilisateur: nom d'équipe`. Les espaces comptent aussi
comme terminaux.

Les variables peuvent changer de valeur selon ce que l'utilisateur tape. Par exemple, `: nom d'utilisateur`
et `: teamname`.

Les variables sont définies comme `: nom: type`. Le `nom` est utilisé dans la documentation d'aide.
Le `type` est utilisé pour faire correspondre l'entrée. Si le type n'est pas donné, alors le type est
`mot`.

Les types valides sont:

* `word` - par défaut. Toute chaîne sans espaces.
* `int` - N'importe quel nombre entier / entier, sans décimales.
* `nombre` - N'importe quel nombre, y compris les nombres entiers et décimaux.
* `pos` - 1,2,3 ou 1,1,2,3.4567 ou (1,2,3) ou 1.2, 2, 3.2
* `text` - N'importe quelle chaîne. Il ne peut y avoir qu'une seule variable de texte,
              aucune variable ou borne ne peut venir après.

Dans `: nom: hp: int`, il y a deux variables:

* `nom` - type de` mot` car aucun type n'est spécifié. Accepte toute chaîne sans espaces.
* `hp` - type de` int`

## Fonctions de sous-commande

Le premier argument est le nom de l'appelant. Les variables sont ensuite transmises au
fonctionner dans l'ordre.

```lua
cmd:sub(":target :hp:int", function(name, target, hp)
    -- subcommand function
end)
```

## Installation de ChatCmdBuilder

Le code source peut être trouvé et téléchargé sur
[Github](https://github.com/rubenwardy/ChatCmdBuilder/).

Il existe deux façons d'installer:

1. Installez ChatCmdBuilder en tant que mod et dépendez-en.
2. Incluez le fichier init.lua dans ChatCmdBuilder en tant que chatcmdbuilder.lua dans votre mod,
    et le déposer.

## Commande complexe admin

Voici un exemple qui crée une commande de chat qui nous permet de le faire:

* `/ admin kill <username>` - tuer l'utilisateur
* `/ admin déplace <username> vers <pos>` - téléporte l'utilisateur
* `/ admin log <username>` - affiche le journal des rapports
* `/ admin log <username> <message>` - log to report log

```lua
local admin_log
local function load()
    admin_log = {}
end
local function save()
    -- todo
end
load()

ChatCmdBuilder.new("admin", function(cmd)
    cmd:sub("kill :name", function(name, target)
        local player = minetest.get_player_by_name(target)
        if player then
            player:set_hp(0)
            return true, "Killed " .. target
        else
            return false, "Unable to find " .. target
        end
    end)

    cmd:sub("move :name to :pos:pos", function(name, target, pos)
        local player = minetest.get_player_by_name(target)
        if player then
            player:setpos(pos)
            return true, "Moved " .. target .. " to " ..
                    minetest.pos_to_string(pos)
        else
            return false, "Unable to find " .. target
        end
    end)

    cmd:sub("log :username", function(name, target)
        local log = admin_log[target]
        if log then
            return true, table.concat(log, "\n")
        else
            return false, "No entries for " .. target
        end
    end)

    cmd:sub("log :username :message", function(name, target, message)
        local log = admin_log[target] or {}
        table.insert(log, message)
        admin_log[target] = log
        save()
        return true, "Logged"
    end)
end, {
    description = "Admin tools",
    privs = {
        kick = true,
        ban = true
    }
})
```

---
title: Débuter
layout: default
root: ../..
idx: 1.1
description: Apprenez à faire un dossier de mod, incluant le lua, mod.conf et autres.
redirect_from:
- /fr/chapters/les_bases.html
- /fr/les_bases/debuter.html
---

## Introduction <!-- omit in toc -->

Comprendre les structures basiques d'un dossier mod est un prérequis essentiel quand on veut créer un mod.

- [Que sont les Jeux et les Mods ?](#que-sont-les-jeux-et-les-mods)
- [Où sont stockés les mods ?](#où-sont-stockés-les-mods)
- [Répertoire Mod](#répertoire-mod)
- [Dépendences](#dépendences)
  - [mod.conf](#modconf)
  - [depends.txt](#dependstxt)
- [Packs de Mods](#packs-de-mods)
- [Exemple](#exemple)
  - [Dossier Mod](#dossier-folder)
  - [depends.txt](#dependstxt-1)
  - [init.lua](#initlua)
  - [mod.conf](#modconf-1)


## Que sont les Jeux et les Mods ?

La puissance de Minetest est la possibilité de créer des jeux facilement sans avoir besoin de créer vos propres voxels graphiques, algorithmes de voxel ou code de réseautage fantaisiste.

Dans Minetest, un jeu est une collection de modules qui travaillent ensemble pour fournir le contenu et la conduite d'un jeu.
Un module, souvent appelé « mod », est un ensemble de scripts et de ressources.
Il est possible de faire un jeu en n'utilisant qu'un seul mod, mais c'est rarement le cas puisque cela réduit considérablement l'intérêt du jeu et sa modularité.

Il est aussi possible de proposer des mods en dehors d’un jeu. Dans ce cas ce sont aussi des *mods*, mais dans un sens plus traditionnel – des modifiacations. Globalement, il faut retenir que les mods ajustent ou ajoutent des fonctionnalités à un jeu.
il est aussi possible de proposer des mods en dehors d’un jeu, dans ce cas ce sont aussi des *mods* dans un sens plus traditionnel – des modifications. Ces mods ajustent ou ajoutent des fonctionnalités à un jeu.

Les mods contenus dans un jeu ainsi que les mods tiers utilisent la même API.

Ce livre va couvrir les parties principales de l'API Minetest, elle est applicable aussi bien pour les developpeurs de jeux que pour les moddeurs.

## Où sont stockés les mods ?

<a name="mod-locations"></a>

Chaque mod a son propre répertoire : y sont localisés le code Lua, les textures, les modèles et les sons. Minetest cherche dans un certains nombre d'endroits les mods. Ces endroits s'appellent couramment "mod load paths" (=Chemins de chargement de mods).

Pour un monde/jeu sauvergardé, 3 endroits sont verifiés, dans l'ordre de priorité suivant :

1. Mods de jeu. Ce sont les mods qui crééent le jeu que le monde fait tourner.
   Ex : `minetest/games/minetest_game/mods/`, `/usr/share/minetest/games/minetest/`
2. Les mods globaux, l'endroit où les mods sont stockés en général.
   Si vous avez un doute, placez-les ici.
   Ex : `minetest/mods/`
3. Les mods du monde, l'endroit où stocker les mods spécifiques à un monde en particulier.
   Ex : `minetest/worlds/world/worldmods/`

Minetest va vérifier les endroits dans l'ordre ci-dessus. S'il rencontre un mod avec le même nom qu'un précédement trouvé, le dernier mod trouvé sera chargé.
Cela veut dire que vous pouvez substituer les mods de jeu en plaçant un mod avec le même nom à l'endroit des mods globaux.

L'endroit effectif de chaque chemin de chargement des mods dépend du système d'exploitation que vous utilisez et de comment vous avez installé Minetest.

* **Windows:**
    * Pour une version portable, ie : depuis un fichier .zip, allez simplement dans le répertoire où vous avez extrait le zip et cherchez les dossiers `games`, `mods` et `worlds`
    * Pour une version installée, ie : depuis un setup.exe, regardez dans C:\\\\Minetest or C:\\\\Games\\Minetest.
* **GNU/Linux:**
    * Pour une installation système regardez dans `~/.minetest`.
      Notez que `~` vaut pour le répertoire home de l'utilisateur, et que les dossiers et fichiers commençant par un point(`.`) sont cachés.
    * Pour une version portable, regardez dans le dossier extrait.
    * Pour une installations Flatpak, cherchez dans `~/.var/app/net.minetest.Minetest/.minetest/mods/`.
* **MacOS**
    * Cherchez dans `~/Library/Application Support/minetest/`.
      Notez que `~` vaut pour le répertoire home de utilisateur, ie: `/Users/USERNAME/`.

## Répertoire Mod

![Trouver le répertoire des mods]({{ page.root }}/static/folder_modfolder.jpg)

Un *nom de mod* est utilisé pour se référer au mod. Chaque mod doit avoir un nom unique.
Les noms de mods peuvent inclure des lettres, chiffres et underscore. Un bon nom doit décrire ce que fait le mod et le dossier qui contient les composants d'un mod doit obligatoirement avoir le même nom que le mod.
Pour savoir si un nom de mod est disponible, essayez des le chercher dans 
[content.minetest.net](https://content.minetest.net).

    monmod
    ├── init.lua (requis) - Se lance au chargement du jeu
    ├── mod.conf (recommandé) - Contient la description et les dépendances du mod. 
    ├── textures (optionnel)
    │   └── ... toutes textures ou images
    ├── sounds (optionnel)
    │   └── ... tout son
    └── ... tout autre fichier ou dossier

Seul le fichier init.lua est requis pour un mod, pour que celui-ci se lance au lancement du jeu.
Cependant, le fichier mod.conf est recommandé et d'autres composants peuvent être nécessaire dépendamment des fonctionnalité du mod.

## Dépendences

On parle de « dépendances » quand un mod a besoin q'un autre mod soit chargé avant lui-même pour fonctionner correctement.
Un mod peut avoir besoin que le code, les objets, ou autres ressources d'un autre mod soit accessible pour qu'il les utilise.

Il y a deux types de dépendances : Les dépendances optionnelles et les dépendances obligatoires.
Dans les deux cas, le mod doit être chargé en premier. Si le mod dépendu n'est pas disponible, une dépendance obligatoire va faire que le mod échouera à charguer, tandis que pour une dépendance optionnelle, cela peut mener à ce qu'il y ait moins de fonctionnalité activées.

Une dépendance optionnelle peut très utile si vous voulez interagir optionnellement avec un autre mod ;
Cela peut rajouter du contenu additionnel si l'utilisateur souhaite utiliser les deux mods en même temps.

Les dépendances doivent être référencées dans mod.conf.

### mod.conf

Ce fichier est utilisé pour inclure les métadonnées d'un mod, incluant son nom, sa description et d'autres informations. Par exemple :

    name = monmod
    description = Ajoute toto, titi et tata.
    depends = modun, moddeux
    optional_depends = modtrois

### depends.txt

Pour une compatibilité avec les versions 0.4.x de Minetest, plutôt que de simplement spécifier les dépendances dans mod.conf, vous devez les renseigner dans un fichier depends.txt dans lequel vous lisquez toutes les dépendances :

    modun
    moddeux
    modtrois?

Chaque nom de mod a sa propre ligne, les noms de mods suivis d'un point d'interrogation sont les dépendances optionnelles.
Si une dépendance optionnelle est installée, elle est chargée avant le mod ; Sinon, si la dépendance n'est pas installée, le mod charge malgrés tout.
C'est la différence avec les dépendances normales (ie : obligatoires) pour lesquelles le mod actuel ne charge pas si elles ne sont pas satisfaites.

## Packs de Mods

Les mods peuvent être groupés en packs de mods qui permettent d'avoir plusieurs mods intégrés et déplacés ensembles. Ils sont très utile si vous voulez fournir plusieurs mods à un joueur mais ne voulez pas les forcer à télécharger chaque mod individuellement.

    modpack1
    ├── modpack.lua (requis) - signale qu'il s'agit d'un pack de mods.
    ├── mod1
    │   └── ... fichiers mod
    └── monmod (optionnel)
        └── ... fichiers mod

Veuillez noter que le pack de mod n'est pas un « jeu ».
Les jeux ont leur propre structure organisationnelle qui sera expliqué dans le chapitre Jeux.

## Exemple

Voici un exemple qui rassemble tout ensemble :

### Dossier Mod
    monmod
    ├── textures
    │   └── monmod_node.png fichiers
    ├── depends.txt
    ├── init.lua
    └── mod.conf

### depends.txt
    default

### init.lua
```lua
print("Ce fichier va se lancer au chargement !")

minetest.register_node("monmod:block", {
    description = "Ceci est un block",
    tiles = {"monmod_block.png"},
    groups = {cracky = 1}
})
```

### mod.conf
    name = monmod
    descriptions = Ajoute un block
    depends = default

Ce mod a le nom "monmod". Il a trois fichiers texte : init.lua, mod.conf,
and depends.txt.\\
Le script fait paraître un message et enregistre ensuite un block — 
Ce qui sera expliqué dans le chapitre suivant.\\
Il y a une simple dépendance, le 
[mod default](https://content.minetest.net/metapackages/default/), qui est habituellement trouvé dans le jeu Minetest.\\
Il y a aussi une texture pour le block, située dans textures/.

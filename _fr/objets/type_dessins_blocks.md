---
title: Types de dessin des nœuds
layout: default
root: ../..
idx: 2.3
description: Guide de tous les types de dessin, y compris les boîtes à nœuds / boîtes à nœuds et les nœuds maillés.
redirect_from: /en/chapters/node_drawtypes.html
---

## Introduction <!-- omit in toc -->

La méthode par laquelle un nœud est dessiné s'appelle un *drawtype*. Il y a beaucoup de
types de tirage disponibles. Le comportement d'un drawtype peut être contrôlé
en fournissant des propriétés dans la définition du type de nœud. Ces propriétés
sont fixes pour toutes les instances de ce nœud. Il est possible de contrôler certaines propriétés
par nœud en utilisant quelque chose appelé `param2`.

Dans le chapitre précédent, le concept de nœuds et d'éléments a été introduit, mais un
la définition complète d'un nœud n'a pas été donnée. Le monde Minetest est une grille 3D de
postes. Chaque position est appelée un nœud et se compose du type de nœud
(nom) et deux paramètres (param1 et param2). La fonction
`minetest.register_node` est un peu trompeur en ce qu'il ne fait pas
enregistrer un nœud - il enregistre un nouveau *type* de nœud.

Les paramètres de nœud sont utilisés pour contrôler la façon dont un nœud est rendu individuellement.
`param1` est utilisé pour stocker l'éclairage d'un nœud et la signification de
`param2` dépend de la propriété` paramtype2` de la définition du type de nœud.


- [Noeuds cubiques: Normal et Allfaces](#cubic-nodes-normal-and-allfaces)
- [Noeuds en verre](#glasslike-nodes)
  - [Glasslike_Framed](#glasslikeframed)
- [Nœuds Airlike](#airlike-nodes)
- [Éclairage et propagation de la lumière du soleil](#lighting-and-sunlight-propagation)
- [Noeuds liquide](#liquid-nodes)
- [Node Boxes](#node-boxes)
  - [Boîtes de nœuds murales](#wallmounted-node-boxes)
- [Noeuds maillés](#mesh-nodes)
- [Noeuds de type signe](#signlike-nodes)
- [Noeuds végétaux](#plantlike-nodes)
- [Noeuds Firelike](#firelike-nodes)
- [Plus de Drawtypes](#more-drawtypes)


## Noeuds cubiques: Normal et Allfaces

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_normal.png" alt="Normal Drawtype">
    <figcaption>
        Normal Drawtype
    </figcaption>
</figure>

Le type de dessin normal est généralement utilisé pour rendre un nœud cubique.
Si le côté d'un nœud normal est contre un côté solide, ce côté ne sera pas rendu,
résultant en un grand gain de performances.

En revanche, le type de dessin allfaces rendra toujours le côté intérieur lorsqu'il est contre
un nœud solide. C'est bon pour les nœuds avec des côtés partiellement transparents, tels que
nœuds foliaires. Vous pouvez utiliser le type de dessin allfaces_optional pour permettre aux utilisateurs de se désinscrire
du dessin plus lent, auquel cas il agira comme un nœud normal.

```lua
minetest.register_node("mymod:diamond", {
    description = "Alien Diamond",
    tiles = {"mymod_diamond.png"},
    groups = {cracky = 3},
})

minetest.register_node("default:leaves", {
    description = "Leaves",
    drawtype = "allfaces_optional",
    tiles = {"default_leaves.png"}
})
```

Remarque: le type de dessin normal est le type de dessin par défaut, vous n'avez donc pas besoin
spécifiez-le.


## Noeuds en verre

La différence entre les nœuds en verre et normaux est que le placement d'un nœud en verre
à côté d'un nœud normal ne masquera pas le côté du nœud normal.
Ceci est utile car les nœuds en verre ont tendance à être transparents, et donc à l'aide d'un
drawtype permettrait de voir à travers le monde.

<figure>
    <img src="{{ page.root }}//static/drawtype_glasslike_edges.png" alt="Glasslike's Edges">
    <figcaption>
        Glasslike's Edges
    </figcaption>
</figure>

```lua
minetest.register_node("default:obsidian_glass", {
    description = "Obsidian Glass",
    drawtype = "glasslike",
    tiles = {"default_obsidian_glass.png"},
    paramtype = "light",
    is_ground_content = false,
    sunlight_propagates = true,
    sounds = default.node_sound_glass_defaults(),
    groups = {cracky=3,oddly_breakable_by_hand=3},
})
```

### Nœuds Airlike

Cela fait que le bord du nœud fait le tour du tout avec un effet 3D, plutôt
que les nœuds individuels, comme suit:

<figure>
    <img src="{{ page.root }}//static/drawtype_glasslike_framed.png" alt="Glasslike_framed's Edges">
    <figcaption>
        Glasslike_Framed's Edges
    </figcaption>
</figure>

Vous pouvez utiliser le type de dessin glasslike_framed_optional pour permettre à l'utilisateur de * s'inscrire *
à l'apparence encadrée.

```lua
minetest.register_node("default:glass", {
    description = "Glass",
    drawtype = "glasslike_framed",
    tiles = {"default_glass.png", "default_glass_detail.png"},
    inventory_image = minetest.inventorycube("default_glass.png"),
    paramtype = "light",
    sunlight_propagates = true, -- Sunlight can shine through block
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
    sounds = default.node_sound_glass_defaults()
})
```


## Nœuds Airlike

Ces nœuds ne sont pas rendus et n'ont donc pas de textures.

```lua
minetest.register_node("myair:air", {
    description = "MyAir (you hacker you!)",
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,

    walkable     = false, -- Would make the player collide with the air node
    pointable    = false, -- You can't select the node
    diggable     = false, -- You can't dig the node
    buildable_to = true,  -- Nodes can be replace this node.
                          -- (you can place a node and remove the air node
                          -- that used to be there)

    air_equivalent = true,
    drop = "",
    groups = {not_in_creative_inventory=1}
})
```


## Éclairage et propagation de la lumière du soleil

L'éclairage d'un nœud est stocké dans param1. Afin de savoir comment ombrer
côté nœud, la valeur lumineuse du nœud voisin est utilisée.
Pour cette raison, les nœuds solides n'ont pas de valeurs lumineuses car ils bloquent la lumière.

Par défaut, un type de nœud ne permet pas de stocker la lumière dans les instances de nœud.
Il est généralement souhaitable que certains nœuds tels que le verre et l'air puissent
laissez passer la lumière. Pour ce faire, deux propriétés doivent être définies:

```lua
paramtype = "light",
sunlight_propagates = true,
```

La première ligne signifie que param1 stocke en fait le niveau de lumière.
La deuxième ligne signifie que la lumière du soleil doit passer par ce nœud sans diminuer de valeur.


## Nœuds liquides

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_liquid.png" alt="Liquid Drawtype">
    <figcaption>
        Liquid Drawtype
    </figcaption>
</figure>

Chaque type de liquide nécessite deux définitions de nœuds - une pour la source de liquide, et
un autre pour faire couler le liquide.

```lua
-- Some properties have been removed as they are beyond
--  the scope of this chapter.
minetest.register_node("default:water_source", {
    drawtype = "liquid",
    paramtype = "light",

    inventory_image = minetest.inventorycube("default_water.png"),
    -- ^ this is required to stop the inventory image from being animated

    tiles = {
        {
            name = "default_water_source_animated.png",
            animation = {
                type     = "vertical_frames",
                aspect_w = 16,
                aspect_h = 16,
                length   = 2.0
            }
        }
    },

    special_tiles = {
        -- New-style water source material (mostly unused)
        {
            name      = "default_water_source_animated.png",
            animation = {type = "vertical_frames", aspect_w = 16,
                aspect_h = 16, length = 2.0},
            backface_culling = false,
        }
    },

    --
    -- Behavior
    --
    walkable     = false, -- The player falls through
    pointable    = false, -- The player can't highlight it
    diggable     = false, -- The player can't dig it
    buildable_to = true,  -- Nodes can be replace this node

    alpha = 160,

    --
    -- Liquid Properties
    --
    drowning = 1,
    liquidtype = "source",

    liquid_alternative_flowing = "default:water_flowing",
    -- ^ when the liquid is flowing

    liquid_alternative_source = "default:water_source",
    -- ^ when the liquid is a source

    liquid_viscosity = WATER_VISC,
    -- ^ how fast

    liquid_range = 8,
    -- ^ how far

    post_effect_color = {a=64, r=100, g=100, b=200},
    -- ^ colour of screen when the player is submerged
})
```

Les nœuds fluides ont une définition similaire, mais avec un nom et une animation différents.
Voir default: water_flowing dans le mod par défaut dans minetest_game pour un exemple complet.


## Boîtes de nœuds

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_nodebox.gif" alt="Nodebox drawtype">
    <figcaption>
        Nodebox drawtype
    </figcaption>
</figure>

Les boîtes à nœuds vous permettent de créer un nœud qui n'est pas cubique, mais qui est à la place
autant de cuboïdes que vous le souhaitez.

```lua
minetest.register_node("stairs:stair_stone", {
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
            {-0.5, 0, 0, 0.5, 0.5, 0.5},
        },
    }
})
```

La partie la plus importante est la table des boîtes de nœuds:

```lua
{-0.5, -0.5, -0.5,       0.5,    0,  0.5},
{-0.5,    0,    0,       0.5,  0.5,  0.5}
```

Chaque ligne est un cuboïde qui est joint pour former un seul nœud.
Les trois premiers nombres sont les coordonnées, de -0,5 à 0,5 inclus, de
le coin inférieur avant le plus à gauche, les trois derniers chiffres sont le coin opposé.
Ils sont sous la forme X, Y, Z, où Y est en place.

Vous pouvez utiliser la [NodeBoxEditor](https://forum.minetest.net/viewtopic.php?f=14&t=2840) pour
créer des boîtes de nœuds en faisant glisser les bords, c'est plus visuel que de le faire à la main.


### Boîtes de nœuds murales

Parfois, vous voulez des boîtes de nœuds différentes pour le moment où il est placé sur le sol, le mur ou le plafond comme avec des torches.

```lua
minetest.register_node("default:sign_wall", {
    drawtype = "nodebox",
    node_box = {
        type = "wallmounted",

        -- Ceiling
        wall_top    = {
            {-0.4375, 0.4375, -0.3125, 0.4375, 0.5, 0.3125}
        },

        -- Floor
        wall_bottom = {
            {-0.4375, -0.5, -0.3125, 0.4375, -0.4375, 0.3125}
        },

        -- Wall
        wall_side   = {
            {-0.5, -0.3125, -0.4375, -0.4375, 0.3125, 0.4375}
        }
    },
})
```

## Noeuds maillés

Bien que les boîtiers de nœuds soient généralement plus faciles à fabriquer, ils sont limités en ce que
ils ne peuvent être constitués que de cuboïdes. Les boîtes à nœuds ne sont pas optimisées non plus;
Les faces internes seront toujours rendues même lorsqu'elles sont complètement masquées.

Un visage est une surface plane sur un maillage. Une face intérieure se produit lorsque les visages de deux
différentes boîtes de nœuds se chevauchent, ce qui entraîne la perte de parties du modèle de boîte de nœuds
invisible mais toujours rendu.

Vous pouvez enregistrer un nœud de maillage comme suit:

```lua
minetest.register_node("mymod:meshy", {
    drawtype = "mesh",

    -- Holds the texture for each "material"
    tiles = {
        "mymod_meshy.png"
    },

    -- Path to the mesh
    mesh = "mymod_meshy.b3d",
})
```

Assurez-vous que le maillage est disponible dans un répertoire `models`.
La plupart du temps, le maillage doit être dans le dossier de votre mod, cependant, il est normal de
partagez un maillage fourni par un autre mod dont vous dépendez. Par exemple, un mod qui
ajoute plus de types de meubles peuvent vouloir partager le modèle fourni par une base
meubles mod.


## Noeuds de type signe

Les nœuds semblables à des signes sont des nœuds plats qui peuvent être montés sur les côtés d'autres nœuds.

Malgré le nom de ce type de dessin, les signes n'ont pas tendance à utiliser des signes, mais
utilisez plutôt le type de dessin `nodebox` pour fournir un effet 3D. Le type de tirage «signlike»
est cependant couramment utilisé par les échelles.

```lua
minetest.register_node("default:ladder_wood", {
    drawtype = "signlike",

    tiles = {"default_ladder_wood.png"},

    -- Required: store the rotation in param2
    paramtype2 = "wallmounted",

    selection_box = {
        type = "wallmounted",
    },
})
```


## Noeuds végétaux

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_plantlike.png" alt="Plantlike Drawtype">
    <figcaption>
        Plantlike Drawtype
    </figcaption>
</figure>

Les nœuds en forme de plante dessinent leurs tuiles selon un motif en X.

```lua
minetest.register_node("default:papyrus", {
    drawtype = "plantlike",

    -- Only one texture used
    tiles = {"default_papyrus.png"},

    selection_box = {
        type = "fixed",
        fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 0.5, 6 / 16},
    },
})
```

## Noeuds Firelike

Firelike est similaire à une plante, sauf qu'il est conçu pour "s'accrocher" aux murs
et plafonds.


<figure>
    <img src="{{ page.root }}//static/drawtype_firelike.png" alt="Firelike nodes">
    <figcaption>
        Firelike nodes
    </figcaption>
</figure>

```lua
minetest.register_node("mymod:clingere", {
    drawtype = "firelike",

    -- Only one texture used
    tiles = { "mymod:clinger" },
})
```

## Plus de Drawtypes

Il ne s'agit pas d'une liste exhaustive, il existe plusieurs types, notamment:

* Fencelike
* Enraciné comme une plante - pour les plantes sous-marines
* Raillike - pour les pistes de chariot
* Torchlike - pour les nœuds 2D mur / sol / plafond.
   Les torches de Minetest Game utilisent en fait deux définitions de nœuds différentes de
   nœuds de maillage (par défaut: torch et par défaut: torch_wall).

Comme toujours, lisez la [documentation de l'API Lua](../../lua_api.html#node-drawtypes)
pour la liste complète.

---
title: Operations basiques sur la Carte
layout: default
root: ../..
idx: 3.1
description: Basic operations like set_node and get_node
redirect_from: /en/chapters/environment.html
---

## Introduction <!-- omit in toc -->

In this chapter, you will learn how to perform basic actions on the map.

- [Structure de la map](#map-structure)
- [Lire](#reading)
  - [Lire des noeuds](#reading-nodes)
  - [Trouver des noeuds](#finding-nodes)
- [Ecrire](#writing)
  - [Ecrire des noeuds](#writing-nodes)
  - [Supprimer des noeuds](#removing-nodes)
- [Charger des Blocs](#loading-blocks)
- [Supprimer des Blocs](#deleting-blocks)

## Structure de la map

La carte Minetest est divisée en MapBlocks, chaque MapBlocks étant un cube de taille 16.
Lorsque les joueurs se déplacent sur la carte, les MapBlocks sont créés, chargés et déchargés.
Les zones de la carte qui ne sont pas encore chargées sont remplies de nœuds *ignore*, un infranchissable
nœud d'espace réservé non sélectionnable. L'espace vide est plein de nœuds *air*, un nœud invisible
vous pouvez traverser.

Les blocs de carte chargés sont souvent appelés *blocs actifs*. Les blocs actifs peuvent être
lire ou écrire par des mods ou des joueurs et avoir des entités actives. Le moteur aussi
effectue des opérations sur la carte, telles que la physique des liquides.

MapBlocks peut être chargé à partir de la base de données mondiale ou généré. MapBlocks
sera généré jusqu'à la limite de génération de carte (`mapgen_limit`) qui est définie
à sa valeur maximale, 31000, par défaut. Les MapBlocks existants peuvent cependant être
chargé à partir de la base de données mondiale en dehors de la limite de génération.

## Lire

### Lire des noeuds

Vous pouvez lire la carte une fois que vous avez une position:
```lua
local node = minetest.get_node({ x = 1, y = 3, z = 4 })
print(dump(node)) --> { name=.., param1=.., param2=.. }
```

Si la position est une décimale, elle sera arrondie au nœud conteneur.
La fonction renverra toujours une table contenant les informations du nœud:

* `name` - The node name, which will be *ignore* when the area is unloaded.
* `param1` - See the node definition. This will commonly be light.
* `param2` - See the node definition.

Il convient de noter que la fonction ne chargera pas le bloc contenant si le bloc
est inactif, mais retournera à la place une table avec «nom» étant «ignoré».

Vous pouvez utiliser `minetest.get_node_or_nil` à la place, qui renverra plutôt` nil`
qu'une table dont le nom est «ignore». Cependant, il ne chargera toujours pas le bloc.
Cela peut toujours retourner `ignorer 'si un bloc contient réellement ignorer.
Cela se produira près du bord de la carte tel que défini par la génération de la carte
limit (`mapgen_limit`).

### Trouver des noeuds

Minetest offre un certain nombre de fonctions d'assistance pour accélérer les actions de carte courantes.
Les plus couramment utilisés sont la recherche de nœuds.

Par exemple, disons que nous voulions faire un certain type de plante qui pousse
mieux près de mese; vous devez rechercher tous les nœuds mese à proximité,
et adapter le taux de croissance en conséquence.

```lua
local grow_speed = 1
local node_pos   = minetest.find_node_near(pos, 5, { "default:mese" })
if node_pos then
    minetest.chat_send_all("Node found at: " .. dump(node_pos))
    grow_speed = 2
end
```

Disons, par exemple, que le taux de croissance augmente plus il y a de mese
proche. Vous devez ensuite utiliser une fonction qui peut trouver plusieurs nœuds dans la zone:

```lua
local pos1       = vector.subtract(pos, { x = 5, y = 5, z = 5 })
local pos2       = vector.add(pos, { x = 5, y = 5, z = 5 })
local pos_list   =
        minetest.find_nodes_in_area(pos1, pos2, { "default:mese" })
local grow_speed = 1 + #pos_list
```

Le code ci-dessus ne fait pas tout à fait ce que nous voulons, car il vérifie en fonction de la zone, tandis que
`find_node_near` vérifie en fonction de la plage. Afin de résoudre ce problème, nous allons,
malheureusement, nous devons vérifier manuellement la plage.

```lua
local pos1       = vector.subtract(pos, { x = 5, y = 5, z = 5 })
local pos2       = vector.add(pos, { x = 5, y = 5, z = 5 })
local pos_list   =
        minetest.find_nodes_in_area(pos1, pos2, { "default:mese" })
local grow_speed = 1
for i=1, #pos_list do
    local delta = vector.subtract(pos_list[i], pos)
    if delta.x*delta.x + delta.y*delta.y <= 5*5 then
        grow_speed = grow_speed + 1
    end
end
```

Maintenant, votre code augmentera correctement `grow_speed` en fonction de ces nœuds à portée.
Notez comment nous avons comparé la distance au carré de la position, plutôt que le carré
l'enraciner pour obtenir la distance réelle. En effet, les ordinateurs trouvent carré
les racines coûtent cher en calcul, vous devez donc les éviter autant que possible.

Il existe plus de variantes des deux fonctions ci-dessus, telles que
`find_nodes_with_meta` et` find_nodes_in_area_under_air`, qui fonctionnent de manière similaire
et sont utiles dans d'autres circonstances.

## Ecrire

### Ecrire des noeuds

Vous pouvez utiliser `set_node` pour écrire sur la carte. Chaque appel à set_node provoquera
l'éclairage à recalculer, ce qui signifie que set_node est assez lent pour les grands
nombre de nœuds.

```lua
minetest.set_node({ x = 1, y = 3, z = 4 }, { name = "default:mese" })

local node = minetest.get_node({ x = 1, y = 3, z = 4 })
print(node.name) --> default:mese
```

set_node supprimera toutes les métadonnées ou l'inventaire associés de cette position.
Ce n'est pas souhaitable dans toutes les circonstances, surtout si vous utilisez plusieurs
définitions de nœuds pour représenter un nœud conceptuel. Un exemple de ceci est le
nœud de four - alors que vous le pensez conceptuellement comme un nœud, c'est en fait
deux.

Vous pouvez définir un nœud sans supprimer les métadonnées ou l'inventaire comme ceci:

```lua
minetest.swap_node({ x = 1, y = 3, z = 4 }, { name = "default:mese" })
```

### Supprimer des noeuds

Un nœud doit toujours être présent. Pour supprimer un nœud, vous définissez la position sur «air».

Les deux lignes suivantes supprimeront toutes les deux un nœud et sont toutes les deux identiques:

```lua
minetest.remove_node(pos)
minetest.set_node(pos, { name = "air" })
```

En fait, remove_node appellera set_node avec le nom étant air.

## Charger des Blocs

Vous pouvez utiliser `minetest.emerge_area` pour charger des blocs de carte. La zone d'émergence est asynchrone,
ce qui signifie que les blocs ne seront pas chargés instantanément. Au lieu de cela, ils seront chargés
prochainement, et le rappel sera appelé à chaque fois.

```lua
-- Load a 20x20x20 area
local halfsize = { x = 10, y = 10, z = 10 }
local pos1 = vector.subtract(pos, halfsize)
local pos2 = vector.add     (pos, halfsize)

local context = {} -- persist data between callback calls
minetest.emerge_area(pos1, pos2, emerge_callback, context)
```

Minetest appellera `emerge_callback` chaque fois qu'il charge un bloc, avec certains
informations sur les progrès.

```lua
local function emerge_callback(pos, action,
        num_calls_remaining, context)
    -- On first call, record number of blocks
    if not context.total_blocks then
        context.total_blocks  = num_calls_remaining + 1
        context.loaded_blocks = 0
    end

    -- Increment number of blocks loaded
    context.loaded_blocks = context.loaded_blocks + 1

    -- Send progress message
    if context.total_blocks == context.loaded_blocks then
        minetest.chat_send_all("Finished loading blocks!")
    end
        local perc = 100 * context.loaded_blocks / context.total_blocks
        local msg  = string.format("Loading blocks %d/%d (%.2f%%)",
                context.loaded_blocks, context.total_blocks, perc)
        minetest.chat_send_all(msg)
    end
end
```

Ce n'est pas le seul moyen de charger des blocs; l'utilisation d'un LVM entraînera également la
blocs englobés à charger de manière synchrone.

## Supprimer des Blocs

Vous pouvez utiliser delete_blocks pour supprimer une plage de blocs de carte:

```lua
-- Delete a 20x20x20 area
local halfsize = { x = 10, y = 10, z = 10 }
local pos1 = vector.subtract(pos, halfsize)
local pos2 = vector.add     (pos, halfsize)

minetest.delete_area(pos1, pos2)
```

Cela supprimera tous les blocs de carte de cette zone, *inclus*. Cela signifie que certains
les nœuds seront supprimés en dehors de la zone car ils seront sur un bloc de carte qui se chevauchent
les limites de la zone.

---
title: Chat et commandes
layout: default
root: ../..
idx: 4.2
description: Enregistrement d'une commande de chat et gestion des messages de chat avec register_on_chat_message
redirect_from: /en/chapters/chat.html
cmd_online:
    level: warning
    title: Offline players can run commands
    message: <p>Un nom de joueur est passé à la place d'un objet joueur car les mods
              peut exécuter des commandes au nom de joueurs hors ligne. Par exemple, l'IRC
              bridge permet aux joueurs d'exécuter des commandes sans rejoindre le jeu.</p>

             <p>Assurez-vous donc que vous ne supposez pas que le joueur est en ligne.
              Vous pouvez vérifier en voyant si <pre> minetest.get_player_by_name </pre> renvoie un joueur.</p>

cb_cmdsprivs:
    level: warning
    title: Privileges and Chat Commands
    message: Le privilège de cri n'est pas nécessaire pour qu'un joueur déclenche ce rappel.
              En effet, les commandes de chat sont implémentées dans Lua et ne sont que
              messages de chat qui commencent par un /.

---

## Introduction <!-- omit in toc -->

Les mods peuvent interagir avec le chat des joueurs, y compris
envoyer des messages, intercepter des messages et enregistrer des commandes de chat.

- [Envoi de messages à tous les joueurs](#sending-messages-to-all-players)
- [Envoi de messages à des joueurs spécifiques](#sending-messages-to-specific-players)
- [Commandes de chat](#chat-commands)
- [Sous-commandes complexes](#complex-subcommands)
- [Interception de messages](#intercepting-messages)

## Envoi de messages à tous les joueurs

Pour envoyer un message à chaque joueur du jeu, appelez la fonction chat_send_all.

```lua
minetest.chat_send_all("This is a chat message to all players")
```

Voici un exemple de la façon dont cela apparaît dans le jeu:

    <player1> Look at this entrance
    This is a chat message to all players
    <player2> What about it?

Le message apparaît sur une ligne distincte pour le distinguer du chat des joueurs en jeu.

## Envoi de messages à des joueurs spécifiques

Pour envoyer un message à un joueur spécifique, appelez la fonction chat_send_player:

```lua
minetest.chat_send_player("player1", "This is a chat message for player1")
```

Ce message s'affiche de la même manière que les messages adressés à tous les joueurs, mais
visible uniquement par le joueur nommé, dans ce cas, joueur1.

## Commandes de chat

Pour enregistrer une commande de chat, par exemple `/ foo`, utilisez `register_chatcommand`:

```lua
minetest.register_chatcommand("foo", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "You said " .. param .. "!"
    end,
})
```

Dans l'extrait ci-dessus, `interact` est répertorié comme obligatoire
[privilege] (privileges.html) signifiant que seuls les joueurs avec le privilège `interact` peuvent exécuter la commande.

Les commandes de chat peuvent renvoyer jusqu'à deux valeurs,
le premier étant un booléen indiquant le succès, et le second étant un
message à envoyer à l'utilisateur.

{% include notice.html notice = page.cmd_online%}

## Sous-commandes complexes

Il est souvent nécessaire de créer des commandes de discussion complexes, telles que:

* `/msg <to> <message>`
* `/team join <teamname>`
* `/team leave <teamname>`
* `/team list`

Cela se fait généralement à l'aide de [modèles Lua] (https://www.lua.org/pil/20.2.html).
Les modèles sont un moyen d'extraire des éléments du texte à l'aide de règles.

```lua
local to, msg = string.match(param, "^([%a%d_-]+) (*+)$")
```

Le code ci-dessus implémente `/ msg <to> <message>`. Passons de gauche à droite:

* `^` signifie correspondre au début de la chaîne.
* `()` est un groupe correspondant - tout ce qui correspond aux éléments ici sera
  renvoyé par string.match.
* `[]` signifie accepter les caractères de cette liste.
* `% a` signifie accepter n'importe quelle lettre et`% d` signifie accepter n'importe quel chiffre.
* `[% d% a _-]` signifie accepter n'importe quelle lettre ou chiffre ou `_` ou` --`.
* `+` signifie faire correspondre la chose avant une ou plusieurs fois.
* `*` signifie correspondre à n'importe quel caractère dans ce contexte.
* `$` signifie correspondre à la fin de la chaîne.

En termes simples, le motif correspond au nom (un mot avec uniquement des lettres/chiffres/-/_),
puis un espace, puis le message (un ou plusieurs caractères). Le nom et
sont renvoyés, car ils sont entourés de parenthèses.

C'est ainsi que la plupart des mods implémentent des commandes de chat complexes. Un meilleur guide pour Lua
Les schémas seraient probablement
[tutoriel lua-users.org] (http://lua-users.org/wiki/PatternsTutorial)
ou la [documentation PIL] (https://www.lua.org/pil/20.2.html).

<p class="book_hide">
Il y a aussi une bibliothèque écrite par l'auteur de ce livre qui peut être utilisée
faire des commandes de chat complexes sans motifs appelés
<a href="chat_complex.html">Générateur de commandes de chat</a>.
</p>


## Interception de messages

Pour intercepter un message, utilisez register_on_chat_message:

```lua
minetest.register_on_chat_message(function(name, message)
    print(name .. " said " .. message)
    return false
end)
```

En renvoyant false, vous autorisez l'envoi du message de discussion par défaut
gestionnaire. Vous pouvez réellement supprimer la ligne `return false` et elle
fonctionne de la même façon, car «nil» est renvoyé implicitement et est traité comme faux.

{% include notice.html notice = page.cb_cmdsprivs%}

Vous devez vous assurer de prendre en compte qu'il peut s'agir d'une commande de chat,
ou l'utilisateur peut ne pas avoir «crier».


```lua
minetest.register_on_chat_message(function(name, message)
    if message:sub(1, 1) == "/" then
        print(name .. " ran chat command")
    elseif minetest.check_player_privs(name, { shout = true }) then
        print(name .. " said " .. message)
    else
        print(name .. " tried to say " .. message ..
                " but doesn't have shout")
    end

    return false
end)
```
